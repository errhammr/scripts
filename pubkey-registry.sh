#!/bin/sh

PKR_DATABASE="${PKR_DATABASE:-"$HOME/.pubkey_registry.json"}"
PKR_TEMP_DATABASE="$(mktemp -t pubkey_registry.json.XXXXXXXXXX)"

umask 0177

pkr_usage() {
    cat >&2 <<EOF
Usage: $(basename "$0") COMMAND HOSTNAME [FINGERPRINT]
  COMMAND can be one of the following:
    add
      Adds a new fingerprint of the given HOSTNAME to the database.
      If FINGERPRINT is not given, the one currently used by the server
      is used.
      HOSTNAME is mandatory.
      FINGERPRINT is optional.
    check
      Checks whether the locally stored fingerprint of a given HOSTNAME's
      public key matches the one currently used by the server. If the
      fingerprints match, the 'lastSeen' timestamp in the database is updated
      to the current date and time.
      HOSTNAME is mandatory.
      FINGERPRINT is ignored.
    checkall
      Checks whether all locally stored fingerprints of public keys match
      the ones currently used by the respective servers. If the fingerprints
      of a server match, the 'lastSeen' timestamp for that host in the database
      is updated to the current date and time.
      HOSTNAME is ignored.
      FINGERPRINT is ignored.
    dump
      Prints the contents of the local database in JSON format. If HOSTNAME is
      given, only print the local database entry for that host.
      HOSTNAME is optional.
      FINGERPRINT is ignored.
    getknownhosts
      Prints the hostnames that are in the database.
      HOSTNAME is ignored.
      FINGERPRINT is ignored.
    getlocal
      Prints the locally stored public key fingerprint for a given HOSTNAME.
      HOSTNAME is mandatory.
      FINGERPRINT is ignored.
    getremote
      Prints the public key fingerprint a given HOSTNAME is currently using.
      This does not update the 'lastSeen' timestamp in the database.
      HOSTNAME is mandatory.
      FINGERPRINT is ignored.
    getremotecert
      Prints the certificate fingerprint a given HOSTNAME is currently using.
      This does not update the 'lastSeen' timestamp in the database.
      HOSTNAME is mandatory.
      FINGERPRINT is ignored.
    getremotecertni
      Like 'getremotecert', formatted as RFC 6920 ni:// URI. This does not
      update the 'lastSeen' timestamp in the database.
      HOSTNAME is mandatory.
      FINGERPRINT is ignored.
    getremotedetails
      Prints human readable information about the server's currently used
      certificate. This does not update the 'lastSeen' timestamp in the
      database.
      HOSTNAME is mandatory.
      FINGERPRINT is ignored.
    getremoteni
      Like 'getremote', formatted as RFC 6920 ni:// URI. This does not update
      the 'lastSeen' timestamp in the database.
      HOSTNAME is mandatory.
      FINGERPRINT is ignored.
    remove
      Removes a HOSTNAME and its fingerprint from the database.
      HOSTNAME is mandatory.
      FINGERPRINT is ignored.
    replace
      Replaces an existing fingerprint of the given HOSTNAME to the database.
      If FINGERPRINT is not given, the one currently used by the server
      is used. If the new fingerprint is different from the old one, the
      'firstSeen' and 'lastSeen' timestamps in the database will bet set to the
      current date and time.
      HOSTNAME is mandatory.
      FINGERPRINT is optional.
    snapshotcertni
      Creates a list of RFC 6920 ni:// URIs of the fingerprints of the currently
      used certificates of all known hosts, including a timestamp. This does not
      update the 'lastSeen' timestamp in the database.
    snapshotni
      Creates a list of RFC 6920 ni:// URIs of the fingerprints of the currently
      used public keys of all known hosts, including a timestamp. This does not
      update the 'lastSeen' timestamp in the database.
  Miscellaneous:
    Only one FINGERPRINT can be stored per HOSTNAME.
    You can specify the location of the local database file using the
      environment variable PKR_DATABASE.
EOF
}

pkr_fetch_server_pubkey_fingerprint() (
    PKR_HOSTNAME="${1:?}"
    PKR_PORT="${2:-"443"}"
    PKR_SERVER_FINGERPRINT="$(openssl s_client -connect "${PKR_HOSTNAME}:${PKR_PORT}" </dev/null 2>/dev/null | \
        openssl x509 -noout -pubkey | \
        openssl asn1parse -noout -inform pem -out - | \
        openssl dgst -sha256 -binary - | \
        openssl enc -base64)"
    # verify we got an actual fingerprint by comparing to the SHA256 of the empty string
    if [ "47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=" = "${PKR_SERVER_FINGERPRINT}" ]; then
        echo "ERROR: Unable to fetch fingerprint from server '${PKR_HOSTNAME}'" >&2
        exit 1
    else
        echo "${PKR_SERVER_FINGERPRINT}"
    fi
)

pkr_fetch_server_cert_fingerprint() (
    PKR_HOSTNAME="${1:?}"
    PKR_PORT="${2:-"443"}"
    PKR_SERVER_FINGERPRINT="$(openssl s_client -connect "${PKR_HOSTNAME}:${PKR_PORT}" </dev/null 2>/dev/null | \
        openssl x509 | \
        openssl asn1parse -noout -inform pem -out - | \
        openssl dgst -sha256 -binary - | \
        openssl enc -base64)"
    # verify we got an actual fingerprint by comparing to the SHA256 of the empty string
    if [ "47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=" = "${PKR_SERVER_FINGERPRINT}" ]; then
        echo "ERROR: Unable to fetch fingerprint from server '${PKR_HOSTNAME}'" >&2
        exit 1
    else
        echo "${PKR_SERVER_FINGERPRINT}"
    fi
)

pkr_fetch_server_cert_details() {
    PKR_HOSTNAME="${1:?}"
    PKR_PORT="${2:-"443"}"
    openssl s_client -connect "${PKR_HOSTNAME}:${PKR_PORT}" </dev/null 2>/dev/null | \
        openssl x509 -noout -text
}

pkr_fetch_server_cert_fingerprint_ni() (
    PKR_HOSTNAME="${1:?}"
    PKR_B64URL_SHA256="$(pkr_fetch_server_cert_fingerprint "${PKR_HOSTNAME}" | tr '+/' '-_' | tr -d '=')"
    if [ -n "${PKR_B64URL_SHA256}" ]; then
        echo "ni://${PKR_HOSTNAME}/sha-256;${PKR_B64URL_SHA256}?hint=cert-fingerprint&timestamp=$(date -u +%Y%m%dT%H%M%SZ)"
    fi
)

pkr_fetch_server_pubkey_fingerprint_ni() (
    PKR_HOSTNAME="${1:?}"
    PKR_B64URL_SHA256="$(pkr_fetch_server_pubkey_fingerprint "${PKR_HOSTNAME}" | tr '+/' '-_' | tr -d '=')"
    if [ -n "${PKR_B64URL_SHA256}" ]; then
        echo "ni://${PKR_HOSTNAME}/sha-256;${PKR_B64URL_SHA256}?hint=pubkey-fingerprint&timestamp=$(date -u +%Y%m%dT%H%M%SZ)"
    fi
)

pkr_get_host_fingerprint_from_db() (
    PKR_HOSTNAME="${1:?}"
    jq -r '."'"${PKR_HOSTNAME}"'".fingerprint' <"$PKR_DATABASE"
)

pkr_command_add() (
    umask 077
    if [ -z "${1}" ]; then
        echo "ERROR: Missing HOSTNAME" >&2
        pkr_usage
        exit 1
    fi
    PKR_EXISTING_FINGERPRINT="$(pkr_get_host_fingerprint_from_db "${1}")"
    if [ "null" = "${PKR_EXISTING_FINGERPRINT}" ]; then
        # fingerprint does not yet exist in the database and can be added
        PKR_NEW_FINGERPRINT="${2:-"$(pkr_fetch_server_pubkey_fingerprint "${1}")"}"
        if [ -z "${PKR_NEW_FINGERPRINT}" ]; then
            echo "ERROR: Unable to determine new fingerprint" >&2
            exit 1
        fi
        NOW="$(date -u +%Y-%m-%dT%H:%M:%S+00:00)"
        jq -S --arg fp "${PKR_NEW_FINGERPRINT}" --arg fs "${NOW}" --arg ls "${NOW}" '. += {"'"${1}"'": {"algo": "sha256", "fingerprint": $fp, "firstSeen": $fs, "lastSeen": $ls}}' <"$PKR_DATABASE" >"$PKR_TEMP_DATABASE"
        mv "$PKR_TEMP_DATABASE" "$PKR_DATABASE"
        echo "Added fingerprint '${PKR_NEW_FINGERPRINT}' for host '${1}'"
    else
        # fingerprint already exists and cannot be added
        echo "ERROR: HOSTNAME already exists in database. Use the 'replace' command to replace it with a new one." >&2
        exit 1
    fi
)

pkr_command_check() (
    if [ -z "${1}" ]; then
        echo "ERROR: Missing HOSTNAME" >&2
        pkr_usage
        exit 1
    fi
    PKR_LOCAL_FINGERPRINT="$(pkr_get_host_fingerprint_from_db "${1}")"
    if [ "null" != "${PKR_LOCAL_FINGERPRINT}" ]; then
        # fingerprint exists in the database and can be checked
        PKR_SERVER_FINGERPRINT="$(pkr_fetch_server_pubkey_fingerprint "${1}")"
        if [ -z "${PKR_SERVER_FINGERPRINT}" ]; then
            echo "ERROR: Unable to fetch server fingerprint" >&2
            return 1
        fi
        if [ "${PKR_LOCAL_FINGERPRINT}" = "${PKR_SERVER_FINGERPRINT}" ]; then
            NOW="$(date -u +%Y-%m-%dT%H:%M:%S+00:00)"
            PKR_EXISTING_FIRSTSEEN="$(jq -r '."'"${1}"'".firstSeen' <"$PKR_DATABASE")"
            jq -S --arg fp "${PKR_LOCAL_FINGERPRINT}" --arg fs "${PKR_EXISTING_FIRSTSEEN}" --arg ls "${NOW}" '. += {"'"${1}"'": {"algo": "sha256", "fingerprint": $fp, "firstSeen": $fs, "lastSeen": $ls}}' <"$PKR_DATABASE" >"$PKR_TEMP_DATABASE"
            mv "$PKR_TEMP_DATABASE" "$PKR_DATABASE"
            echo "[OK]  '${1}'  Server's public key fingerprint hasn't changed since ${PKR_EXISTING_FIRSTSEEN}"
            return 0
        else
            PKR_EXISTING_LASTSEEN="$(jq -r '."'"${1}"'".lastSeen' <"$PKR_DATABASE")"
            echo "[MISMATCH]  '${1}'  Server's public key fingerprint DOES NOT match the locally stored one which was last seen ${PKR_EXISTING_LASTSEEN}"
            return 1
        fi
    else
        echo "ERROR: HOSTNAME does not exist in database. Use the 'add' command to add it." >&2
        return 1
    fi
)

pkr_list_known_hosts() {
    jq -r 'keys[]' <"${PKR_DATABASE}"
}

pkr_command_checkall() (
    PKR_STATUS_CODE=0
    while read -r PKR_HOST; do
        pkr_command_check "${PKR_HOST}"
        PKR_STATUS_CODE=$(( PKR_STATUS_CODE + $? ))
    done <<EOF
$(pkr_list_known_hosts)
EOF
    if [ $PKR_STATUS_CODE -lt 128 ]; then
        return $PKR_STATUS_CODE
    else
        return 128
    fi
)

pkr_command_remove() (
    if [ -z "${1}" ]; then
        echo "ERROR: Missing HOSTNAME" >&2
        pkr_usage
        exit 1
    fi
    PKR_EXISTING_FINGERPRINT="$(pkr_get_host_fingerprint_from_db "${1}")"
    if [ "null" != "${PKR_EXISTING_FINGERPRINT}" ]; then
        # fingerprint exists in the database and can be removed
        jq -S 'del(."'"${1}"'")' <"$PKR_DATABASE" >"$PKR_TEMP_DATABASE"
        mv "$PKR_TEMP_DATABASE" "$PKR_DATABASE"
        echo "Removed fingerprint '${PKR_EXISTING_FINGERPRINT}' and host '${1}'"
    else
        echo "ERROR: HOSTNAME does not exist in database. Use the 'add' command to add it." >&2
        exit 1
    fi
)

pkr_command_replace() (
    if [ -z "${1}" ]; then
        echo "ERROR: Missing HOSTNAME" >&2
        pkr_usage
        exit 1
    fi
    PKR_EXISTING_FINGERPRINT="$(pkr_get_host_fingerprint_from_db "${1}")"
    if [ "null" != "${PKR_EXISTING_FINGERPRINT}" ]; then
        # fingerprint exists in the database and can be replaced
        PKR_NEW_FINGERPRINT="${2:-"$(pkr_fetch_server_pubkey_fingerprint "${1}")"}"
        if [ -z "${PKR_NEW_FINGERPRINT}" ]; then
            echo "ERROR: Unable to determine new fingerprint" >&2
            exit 1
        fi
        if [ "${PKR_EXISTING_FINGERPRINT}" = "${PKR_NEW_FINGERPRINT}" ]; then
            echo "Old and new fingerprints are the same. Nothing to do for host '${1}'"
        else
            NOW="$(date -u +%Y-%m-%dT%H:%M:%S+00:00)"
            jq -S --arg fp "${PKR_NEW_FINGERPRINT}" --arg fs "${NOW}" --arg ls "${NOW}" '. += {"'"${1}"'": {"algo": "sha256", "fingerprint": $fp, "firstSeen": $fs, "lastSeen": $ls}}' <"$PKR_DATABASE" >"$PKR_TEMP_DATABASE"
            mv "$PKR_TEMP_DATABASE" "$PKR_DATABASE"
            echo "Replaced old fingerprint '${PKR_EXISTING_FINGERPRINT}' with new fingerprint '${PKR_NEW_FINGERPRINT}' for host '${1}'"
        fi
    else
        echo "ERROR: HOSTNAME does not exist in database. Use the 'add' command to add it." >&2
        exit 1
    fi
)

pkr_command_snapshotni() {
    for pkr_host in $(pkr_list_known_hosts); do 
        pkr_fetch_server_pubkey_fingerprint_ni "$pkr_host"
    done
}

pkr_command_snapshotcertni() {
    for pkr_host in $(pkr_list_known_hosts); do 
        pkr_fetch_server_cert_fingerprint_ni "$pkr_host"
    done
}

PKR_COMMAND="${1}"

if [ ! -f "${PKR_DATABASE}" ]; then
    echo '{}' >"${PKR_DATABASE}"
fi

case $PKR_COMMAND in
    "add" ) pkr_command_add "${2}" "${3}";;
    "check" ) pkr_command_check "${2}";;
    "checkall" ) pkr_command_checkall;;
    "dump" ) if [ -z "${2}" ]; then jq -S <"$PKR_DATABASE"; else jq -S 'to_entries[] | select(.key == "'"${2}"'") | {(.key): .value}' <"$PKR_DATABASE"; fi;;
    "getknownhosts" ) pkr_list_known_hosts;;
    "getlocal" ) pkr_get_host_fingerprint_from_db "${2}";;
    "getremote" ) pkr_fetch_server_pubkey_fingerprint "${2}";;
    "getremotecert" ) pkr_fetch_server_cert_fingerprint "${2}";;
    "getremotecertni" ) pkr_fetch_server_cert_fingerprint_ni "${2}";;
    "getremotedetails" ) pkr_fetch_server_cert_details "${2}";;
    "getremoteni" ) pkr_fetch_server_pubkey_fingerprint_ni "${2}";;
    "remove" ) pkr_command_remove "${2}";;
    "replace" ) pkr_command_replace "${2}" "${3}";;
    "snapshotcertni" ) pkr_command_snapshotcertni;;
    "snapshotni" ) pkr_command_snapshotni;;
    * ) pkr_usage;;
esac

PKR_EXIT_CODE="$?"
[ -f "${PKR_TEMP_DATABASE}" ] && rm "${PKR_TEMP_DATABASE}"
exit $PKR_EXIT_CODE
