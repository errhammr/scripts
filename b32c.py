#!/usr/bin/env python3

import base64, re

class B32C:
    """
The code in this class is based on https://github.com/ingydotnet/crockford-py

Copyright (c) 2011, Ingy dot Net
All rights reserved.

Redistribution and use in source and binary forms, with or
without modification, are permitted provided that the following
conditions are met:

1) Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2) Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Ingy dot Net AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Ingy dot Net OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation
are those of the authors and should not be interpreted as representing
official policies, either expressed or implied, of Ingy dot Net.
    """

    __std2crock = str.maketrans(
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567",
        "0123456789ABCDEFGHJKMNPQRSTVWXYZ",
        '='
    )
    __crock2std = str.maketrans(
        "0123456789ABCDEFGHJKMNPQRSTVWXYZOIL",
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567ABB",
        '='
    )

    def b32encode(s):
        return base64.b32encode(s).decode().translate(B32C.__std2crock)

    def b32decode(b32, casefold=None, map01=None):
        # Ensure the manatory padding is correct:
        b32 += '=' * ((8 - len(b32) % 8) % 8)
        return base64.b32decode(b32.upper().translate(B32C.__crock2std),
            casefold=casefold, map01=map01)

class Luhn:
    def luhnModN(s: str, alphabet: str) -> str:
        """
        returns the Luhn mod N check digit of a string and a given alphabet
        """
        count = 0
        luhnVal = 0
        multiplyOn = (len(s) + 1) % 2
        alphabetLength = len(alphabet)
        for char in s:
            i = alphabet.find(char)

            if count % 2 == multiplyOn:
                i = i * 2

            i = int(i / alphabetLength) + (i % alphabetLength)
            luhnVal = (luhnVal + i) % alphabetLength
            count = count + 1
        luhnVal = (alphabetLength - (luhnVal % alphabetLength)) % alphabetLength
        return alphabet[luhnVal]

class CB32Code:
    def fromInt(i: int, separator: str = '-') -> str:
        if not isinstance(i, int):
            raise TypeError('i must be int')
        b = i.to_bytes(length=(((max(i.bit_length(), 1) + 7) // 8) // 5 + 1) * 5, byteorder='big')
        bs = B32C.b32encode(b)
        c = Luhn.luhnModN(bs, '0123456789ABCDEFGHJKMNPQRSTVWXYZ')
        code = separator.join(bs[x:x+4] for x in range(0, len(bs), 4)) + separator + c
        return re.sub(r'^(0000' + re.escape(separator) + ')+', '', code)
    
    def fromIntAlternative(i: int, separator: str = '-', firstGroup: int = 2) -> str:
        if not isinstance(i, int):
            raise TypeError('i must be int')
        b = i.to_bytes(length=(((max(i.bit_length(), 1) + 7) // 8) // 5 + 1) * 5, byteorder='big')
        bs = re.sub(r'^0+', '', B32C.b32encode(b))
        c = Luhn.luhnModN(bs, '0123456789ABCDEFGHJKMNPQRSTVWXYZ')
        if firstGroup > 0:
            code = bs[:firstGroup] + separator + bs[firstGroup:] + separator + c
        else:
            code = bs + separator + c
        return code
    
    def toInt(s: str, separator: str = '-') -> int:
        if not isinstance(s, str):
            raise TypeError('s must be str')
        string = s.replace(separator, '').upper().translate(str.maketrans('OIL', '011', '='))
        toBeChecked = string[:-1]
        actualCheckDigit = string[-1:]
        expectedCheckDigit = Luhn.luhnModN(toBeChecked, '0123456789ABCDEFGHJKMNPQRSTVWXYZ')
        if actualCheckDigit != expectedCheckDigit:
            raise ValueError(f'check digit does not match: found {actualCheckDigit} but expected {expectedCheckDigit} in {string}')
        padding = '0' * ((8 - len(toBeChecked) % 8) % 8)
        return int(B32C.b32decode(padding + toBeChecked).hex(), 16)


if __name__ == '__main__':
    import sys

    if len(sys.argv) > 1:
        try:
            i = None
            if sys.argv[1].lower().startswith('0x'):
                i = int(sys.argv[1], 16)
            elif sys.argv[1].lower().startswith('0o'):
                i = int(sys.argv[1], 8)
            else:
                i = int(sys.argv[1])
            if len(sys.argv) >= 4 and sys.argv[2] == '--alt' and re.match(r'^[0-9][0-9]*$', sys.argv[3]):
                print(CB32Code.fromIntAlternative(i, firstGroup=int(sys.argv[3])))
            else:
                print(CB32Code.fromInt(i))#.lstrip('0'))
        except BaseException:
            try:
                i = CB32Code.toInt(sys.argv[1])
                if len(sys.argv) > 2 and sys.argv[2] == '--hex':
                    print(hex(i))
                else:
                    print(i)
            except ValueError as ve:
                print(ve, file=sys.stderr)
        exit(0)

    numbers = (
        1, 2, 3, 4, 5,
        42, 1337, 
        19700101, 
        1981_12_31_01_02_03, 
        int((b'ThisIsRidiculous').hex(), 16)
    )
    for i in numbers:
        sep = '-'
        code = CB32Code.fromInt(i, sep)
        integer = CB32Code.toInt(code, sep)
        print(code, integer, integer == i)
    
    sep = '-'
    code = 'abc-dt'
    integer = CB32Code.toInt(code, sep)
    print(code, integer, CB32Code.fromInt(integer))

    code = '1ZZZZ2'
    integer = CB32Code.toInt(code, sep)
    print(code, integer, CB32Code.fromInt(integer))
