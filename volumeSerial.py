#!/usr/bin/env python3

from base64 import b16encode
from datetime import datetime
import sys

def mkVolumeSerial(d, explain: bool = False) -> str:
    '''
    Generates a Volume Serial Number based on the old DOS algorithm
    that uses the current timestamp.

    Algorithm from
    https://www.digital-detective.net/documents/Volume%20Serial%20Numbers.pdf

    Another test case from
    https://www.betaarchive.com/forum/viewtopic.php?t=29963
    volumeSerial.py "1992-03-20 13:08:14.090" # => 111D-14D0

    See also: FreeDOS format utility:
    function Create_Serial_Number() in createfs.c
    https://github.com/FDOS/format/blob/master/createfs.c
    '''

    if not isinstance(d, datetime):
        raise ValueError('d must be an object of class datetime')

    a1 = (d.month << 8) + d.day
    a2 = (d.second << 8) + int(d.microsecond / 10000)
    a = a1 + a2

    b1 = (d.hour << 8) + d.minute
    b2 = d.year
    b = b1 + b2

    if explain:
        print( 'Volume Serial for timestamp ' + d.isoformat(), file=sys.stderr)
        print( 'mon   day   sec   1/10s   hour   min   year', file=sys.stderr)
        print(f'{d.month:0>-2}    {d.day:0>-2}    {d.second:0>-2}    {int(d.microsecond / 10000):0>-2}      {d.hour:0>-2}     {d.minute:0>-2}    {d.year:0>-4}   (b10)', file=sys.stderr)
        print(f'{d.month:0>-2X}    {d.day:0>-2X}    {d.second:0>-2X}    {int(d.microsecond / 10000):0>-2X}      {d.hour:0>-2X}     {d.minute:0>-2X}    {d.year:0>-4X}   (b16)', file=sys.stderr)
        print(f'╰─╮ ╭─╯     ╰─╮ ╭─╯       ╰─╮ ╭──╯     │', file=sys.stderr)
        print(f'  {d.month:0>-2X}{d.day:0>-2X}        {d.second:0>-2X}{int(d.microsecond / 10000):0>-2X}          {d.hour:0>-2X}{d.minute:0>-2X}       {d.year:0>-4X}', file=sys.stderr)
        print(f'     ╰─╴+╶────╯                ╰─╴+╶───╯', file=sys.stderr)
        print(f'        │                         │', file=sys.stderr)
        print(f'        {a:0>-4X}                      {b:0>-4X}', file=sys.stderr)
        print(f'╭───────╯                         │', file=sys.stderr)
        print(f'│    ╭────────────────────────────╯', file=sys.stderr)

    return b16encode(a.to_bytes(2, 'big')).decode().upper() + '-' + b16encode(b.to_bytes(2, 'big')).decode().upper()

if __name__ == '__main__':
    try:
        d = datetime.now()
        explain: bool = False
        if len(sys.argv) > 1 and sys.argv[1] != '-e':
            d = datetime.fromisoformat(sys.argv[1])
        if sys.argv[len(sys.argv)-1] == '-e':
            explain = True
        print(mkVolumeSerial(d, explain))
    except BaseException as e:
        print(e, file=sys.stderr)
        print(f'Usage: volumeSerial.py [ISO8601-Timestamp]', file=sys.stderr)
