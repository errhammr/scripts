#!/bin/sh

set -e

# prints all MAC addresses of a system as dev URNs

# see RFC 9039 section 4.1 for more information
# https://www.rfc-editor.org/rfc/rfc9039.html#name-mac-addresses

# tested on the platforms listed in print_tested_platforms()

print_tested_platforms() (
    cat >&2 <<EOF
  Tested platforms:
    Linux
      openSUSE Tumbleweed 20230508
      Ubuntu 20.04
    OpenBSD 7.3
EOF
)

get_macs_openbsd_73() (
    while read -r LLADDR; do
        echo "${LLADDR}" | cut -d ' ' -f2 | tr -d ':'
    done <<EOF
$(ifconfig | grep lladdr)
EOF
)

get_macs_linux_ip() (
    ip addr show | grep 'link/ether' | cut -d ' ' -f6 | tr -d ':'
)

get_macs() {
    KERNEL_NAME="$(uname -s)"
    case "${KERNEL_NAME}" in
        "OpenBSD")
            OBSD_RELEASE="$(uname -r)"
            if [ "${OBSD_RELEASE}" != "7.3" ]; then
                echo "Warning: this script was not tested on OpenBSD ${OBSD_RELEASE} and might not work as intended on your machine." >&2
                print_tested_platforms
            fi
            get_macs_openbsd_73
            ;;
        "Linux")
            if command -v lsb_release >/dev/null 2>&1; then
                DISTRO_SUPPORTED=0
                DISTRO_NAME="$(lsb_release --short --id)"
                DISTRO_VERSION="$(lsb_release --short --release)"
                [ "${DISTRO_NAME}" = "Ubuntu" ] && [ "${DISTRO_VERSION}" = "20.04" ] && DISTRO_SUPPORTED=1
                [ "${DISTRO_NAME}" = "openSUSE" ] && [ "$(lsb_release --short --description)" = '"openSUSE Tumbleweed"' ] && DISTRO_SUPPORTED=1
                if [ "${DISTRO_SUPPORTED}" -ne 1 ]; then
                    echo "Warning: this script was not tested on ${DISTRO_NAME} ${DISTRO_VERSION} and might not work as intended on your machine." >&2
                fi
                if command -v ip >/dev/null 2>&1; then
                    get_macs_linux_ip
                else
                    echo "Error: 'ip' command not found. Your Linux distribution is not supported by this script." >&2
                    print_tested_platforms
                    exit 1
                fi
            else
                echo "Error: 'lsb_release' command not found. Your Linux distribution is not supported by this script." >&2
                print_tested_platforms
                exit 1
            fi
            ;;
        *)
            echo "Error: ${KERNEL_NAME} is not supported." >&2
            print_tested_platforms
            exit 1
            ;;
    esac
}

while read -r MAC; do
    EUI64="$(printf "%s" "${MAC}" | sed 's/.\{6\}/&ffff/')"
    echo "urn:dev:mac:${EUI64}"
done <<EOF
$(get_macs)
EOF

