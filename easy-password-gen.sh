#!/bin/sh

# generates a password like the following:
# djbbw-xtt44-b7trz-bk7jz-pkpp7
# 25 chars (lower case consonants and digits),
# in 5 groups of 5 chars, delimited by dashes
# 
# goal: resonably secure and easy to type by hand
# passwords should contain about 129 bits of entropy if I
# got the math right

pwgen -AnBsv 25 1 | fold -b5 | sed -z 's/\n/-/g;s/-$/\n/'