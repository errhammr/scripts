#!/usr/bin/python3

# Implements RFC4193
# https://datatracker.ietf.org/doc/html/rfc4193

import time
import hashlib
import uuid

def isMacValid(mac: bytes) -> bool:
	return len(mac) == 6

def mkUla(unix_time: float, mac: bytes, debug: bool = False) -> str:
	if not isMacValid(mac):
		raise Exception('Invalid MAC address')
	seconds_from_1900_to_1970 = 2208988800.0
	ntp_time = int((unix_time + seconds_from_1900_to_1970) * (2**32)).to_bytes(8, byteorder='big')
	eui_64 = m[0:3] + (0xfffe).to_bytes(2, byteorder='big') + m[3:6]
	if debug:
		print('[D] Unix time:', unix_time)
		print('[D] NTP time: ', ntp_time.hex())
		print('[D] MAC:      ', mac.hex())
		print('[D] EUI-64:   ', eui_64.hex())
	hash_input = ntp_time + eui_64
	global_id = hashlib.sha1(hash_input).hexdigest()[30:]
	return 'fd' + global_id[0:2] + ':' + global_id[2:6] + ':' + global_id[6:10] + '::/48'

if __name__ == '__main__':
	success = False
	errorCount = 0
	t = time.time()
	m = (uuid.getnode()).to_bytes(6, byteorder='big')

	if not isMacValid(m):
		print('[I] Unable to get system MAC address')
	else:
		print('[I] Found system MAC address:', m.hex())

	while(not success):
		if not isMacValid(m):
			try:
				print('Please provide a valid MAC address without delimiters (12 hexadecimal chars).')
				print('Example:')
				print('    C7:84:AA:DE:3E:88  might be your MAC address')
				print('    C784AADE3E88       is what you type in')
				m = bytes.fromhex(input('Your MAC address: '))
			except:
				pass
		try:
			print('Your IPv6 ULA:', mkUla(t, m, True))
			success = True
		except Exception as e:
			print('Error:', e)
		errorCount = errorCount + 1
		if errorCount >= 5:
			print('[E] too many failed attempts, aborting')
			break
