#!/bin/sh

# it's WLAN, not WiFi, understood?

RW_SSID="WLAN-$(tr -dc '0123456789ABCDEFGHJKMNPQRSTVWXYZ' </dev/urandom | fold -w6 | head -n1)"
RW_PSK="$(xxd -ps -l 32 -c 256 /dev/urandom | tr '[:lower:]' '[:upper:]')"

printf "WIFI:T:WPA;S:%s;P:%s;;" "${RW_SSID}" "${RW_PSK}" | qrencode -t utf8

echo "Authentication: WPA2-PSK"
echo "SSID:           ${RW_SSID}"
echo "PSK:            ${RW_PSK}"
