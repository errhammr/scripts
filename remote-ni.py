#!/usr/bin/env python3

import base64, hashlib, re, requests, sqlite3
from datetime import datetime, timezone

try:
	import blake3
except ModuleNotFoundError:
	pass

class HashDb:

	def __init__(self, dbFile: str):
		self.__con = sqlite3.connect(dbFile)
		self.__cur = self.__con.cursor()
		self.__cur.execute('''
			CREATE TABLE IF NOT EXISTS "hash_algos" (
				"hash_algo_id"	INTEGER NOT NULL,
				"hash_algo_name"	TEXT NOT NULL UNIQUE,
				PRIMARY KEY("hash_algo_id" AUTOINCREMENT)
			);
		''')
		self.__cur.execute('''
			CREATE TABLE IF NOT EXISTS "hashes" (
				"hash_id"	INTEGER NOT NULL,
				"hash_algo"	INTEGER NOT NULL,
				"hash_value"	BLOB NOT NULL,
				UNIQUE("hash_algo","hash_value"),
				PRIMARY KEY("hash_id" AUTOINCREMENT),
				FOREIGN KEY("hash_algo") REFERENCES "hash_algos"("hash_algo_id")
			);
		''')
		self.__cur.execute('''
			CREATE TABLE IF NOT EXISTS "seen" (
				"url_id"	INTEGER NOT NULL,
				"hash_id"	INTEGER NOT NULL,
				"seen"	TEXT NOT NULL,
				FOREIGN KEY("hash_id") REFERENCES "hashes"("hash_id"),
				FOREIGN KEY("url_id") REFERENCES "urls"("url_id")
			);
		''')
		self.__cur.execute('''
			CREATE TABLE IF NOT EXISTS "urls" (
				"url_id"	INTEGER NOT NULL,
				"url"	TEXT NOT NULL UNIQUE,
				PRIMARY KEY("url_id" AUTOINCREMENT)
			);
		''')
		self.__cur.execute('''
			INSERT OR IGNORE INTO "hash_algos" ("hash_algo_id","hash_algo_name") VALUES (1,'sha-256'),
				(2,'sha-384'),
				(3,'sha-512'),
				(4,'sha3-224'),
				(5,'sha3-256'),
				(6,'sha3-384'),
				(7,'sha3-512'),
				(8,'blake2b-512'),
				(9,'blake2s-256'),
				(10,'blake3-256');
		''')
		self.__con.commit()
	
	def storeScan(self, originalUrl: str, actualUrl: str, hashers: dict):
		now = datetime.now(timezone.utc).isoformat()
		self.__cur.execute('''
            INSERT OR IGNORE INTO `urls` (`url`) VALUES
				(?), (?);
        ''', (
            originalUrl,
			actualUrl
            )
        )
		for hashName, hasher in hashers.items():
			if hasher != None:
				self.__cur.execute('''
					INSERT OR IGNORE INTO `hashes` (`hash_algo`, `hash_value`) VALUES
						(
							(SELECT `hash_algo_id` FROM `hash_algos` WHERE `hash_algo_name` = ?),
							?
						);
				''', (
					hashName,
					hasher.getDigestBytes()
					)
				)
				self.__cur.execute('''
					INSERT INTO `seen` (`url_id`, `hash_id`, `seen`) VALUES
						(
							(SELECT `url_id` FROM `urls` WHERE `url` = ?),
							(SELECT `hash_id` FROM `hashes` WHERE 
								`hash_algo` = (SELECT `hash_algo_id` FROM `hash_algos` WHERE `hash_algo_name` = ?) AND 
								`hash_value` = ?),
							?
						),
						(
							(SELECT `url_id` FROM `urls` WHERE `url` = ?),
							(SELECT `hash_id` FROM `hashes` WHERE 
								`hash_algo` = (SELECT `hash_algo_id` FROM `hash_algos` WHERE `hash_algo_name` = ?) AND 
								`hash_value` = ?),
							?
						);
				''', (
					originalUrl,
					hashName,
					hasher.getDigestBytes(),
					now,
					actualUrl,
					hashName,
					hasher.getDigestBytes(),
					now
					)
				)
		self.__con.commit()

class MinimalUriNi:

	_ni_algo_to_hashlib = {
		'sha-256': 'sha256',
		'sha-384': 'sha384',
		'sha-512': 'sha512',
		'sha3-224': 'sha3_224',
		'sha3-256': 'sha3_256',
		'sha3-384': 'sha3_384',
		'sha3-512': 'sha3_512',
		'blake2s-256': 'blake2s',
		#'blake2b-256': 'blake2b', # this script doesn't handle the initialization of the digest size properly
		'blake2b-512': 'blake2b',
		'blake3-256': 'blake3', # this script doesn't use hashlib for blake3
	}

	__shortPrefixes = {
		# RFC 6920 section 9.4 -> https://datatracker.ietf.org/doc/html/rfc6920#section-9.4
		'sha-256'    : '1',
		'sha-256-128': '2',
		'sha-256-120': '3',
		'sha-256-96' : '4',
		'sha-256-64' : '5',
		'sha-256-32' : '6',
		# IANA registry for Named Information -> https://www.iana.org/assignments/named-information/named-information.xhtml#hash-alg
		'sha-384': '7',
		'sha-512': '8',
		'sha3-224': '9',
		'sha3-256': '10',
		'sha3-384': '11',
		'sha3-512': '12',
		'blake2s-256': None,
		'blake2b-256': None,
		'blake2b-512': None,
		# unofficial
		'blake3-256': None
	}

	def __init__(self, algo: str = 'sha-256'):
		algo = algo.lower()
		if algo in self._ni_algo_to_hashlib:
			self.__hash_algo_base = algo
		else:
			raise ValueError('Unsupported hash algorithm: ' + str(algo))
		self.__hasher = self.__getNewHasher()
		self.__rawHash = b''
	
	def getAlgo(self) -> str:
		return self.__hash_algo_base

	def getDigestBytes(self) -> bytes:
		return self.__rawHash

	def __getNewHasher(self):
		if self._ni_algo_to_hashlib[self.__hash_algo_base] == 'blake3':
			try:
				return blake3.blake3(max_threads=blake3.blake3.AUTO)
			except NameError:
				raise ValueError('Unable to create hasher for ' + self.__hash_algo_base + ': missing module blake3 (pip install blake3)')
		return hashlib.new(self._ni_algo_to_hashlib[self.__hash_algo_base])

	def _rawUpdate(self, rawData):
		self.__hasher.update(rawData)
	
	def _rawFinish(self):
		self.__rawHash = self.__hasher.digest()
	
	def formatNi(self, authority: str = '') -> str:
		algo = self.__hash_algo_base
		hashStr = base64.urlsafe_b64encode(self.__rawHash).decode('utf8').replace('=', '')

		return 'ni://' + authority + '/' + algo + ';' + hashStr
	
	def formatNih(self, authority: str = '', dashEvery: int = 4) -> str:
		algo = self.__hash_algo_base
		hashStr = self.__rawHash.hex()
		hashStrDashed = MinimalUriNi.insertDashes(hashStr, dashEvery)

		if algo in self.__shortPrefixes and self.__shortPrefixes[algo] is not None:
			algo = self.__shortPrefixes[algo]
		nih = 'nih:' + algo + ';' + hashStrDashed

		luhn = MinimalUriNi.luhnModN(hashStr, '0123456789abcdef')
		nih = nih + ';' + luhn

		return nih
	
	def insertDashes(string: str, x: int) -> str:
		if x == 0:
			return string
		new = ''
		cnt = 0
		for ch in string:
			if cnt%x==0 and cnt!=0:
				new += '-'
			cnt += 1
			new += str(ch)
		return new

	def luhnModN(s: str, alphabet: str) -> str:
		"""
		returns the Luhn mod N check digit of a string and a given alphabet
		"""
		count = 0
		luhnVal = 0
		multiplyOn = (len(s) + 1) % 2
		alphabetLength = len(alphabet)
		for char in s:
			i = alphabet.find(char)

			if count % 2 == multiplyOn:
				i = i * 2

			i = int(i / alphabetLength) + (i % alphabetLength)
			luhnVal = (luhnVal + i) % alphabetLength
			count = count + 1
		luhnVal = (alphabetLength - (luhnVal % alphabetLength)) % alphabetLength
		return alphabet[luhnVal]
	
def hashRemoteFile(url: str, dbFile: str = None) -> dict:
	hashers = {}
	headers = {}
	statusCode = 0
	actualUrl = None
	isRedirect = False
	for hashName in sorted(list(MinimalUriNi._ni_algo_to_hashlib.keys())):
		try:
			hashers[hashName] = MinimalUriNi(algo=hashName)
			#print('created hasher for', hashName, file=sys.stderr)
		except:
			pass # ignore failed hashers, continue with what we got
	#print('start fetching:', url, file=sys.stderr)
	with requests.get(url, timeout=5, stream=True) as response:
		headers = response.headers
		statusCode = response.status_code
		actualUrl = response.url
		isRedirect = response.is_redirect
		#print('headers:', headers, file=sys.stderr)
		#print('statusCode:', statusCode, file=sys.stderr)
		#print('progress: ', end='', file=sys.stderr, flush=True)
		for chunk in response.iter_content(16384):
			#print('.', end='', file=sys.stderr, flush=True)
			for h in hashers.values():
				if h != None:
					try:
						h._rawUpdate(chunk)
					except:
						h = None
						#print('destroyed hasher for', hashName, file=sys.stderr)
		#print(' done', file=sys.stderr)
	#print('done fetching:', url, file=sys.stderr)
	remoteInfo = {
		'requestedUrl': url,
		'fetchedUrl': actualUrl,
		'statusCode': statusCode,
		'headers': dict(headers),
		'integrity': {},
	}
	for hashName, hashNiObj in hashers.items():
		if hashNiObj != None:
			try:
				hashNiObj._rawFinish()
				remoteInfo['integrity'][hashName] = hashNiObj.formatNi()
			except:
				pass # ignore failed hashers, continue with what we got
	
	if statusCode == 200 and dbFile != None:
		db = HashDb(dbFile)
		db.storeScan(url, actualUrl, hashers)

	return remoteInfo


	
if __name__ == '__main__':
	
	import json, sys

	dbFile = None
	if len(sys.argv) > 3 and sys.argv[2] == '--db' and sys.argv[3] != '':
		dbFile = sys.argv[3]

	metadata = hashRemoteFile(sys.argv[1], dbFile)
	
	print(json.dumps(
		metadata,
		indent=2
	))
