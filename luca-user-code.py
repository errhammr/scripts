#!/usr/bin/env python3

import hashlib
import struct
import time



# random number generator that doesn't use system entropy
class RandomSequenceOfUnique:
	
	__m_index = 0
	__m_intermediateOffset = 0
	
	def __overflow32(self, x: int) -> int:
		return x & 0xFFFFFFFF
	
	def __overflow64(self, x: int) -> int:
		return x & 0xFFFFFFFFFFFFFFFF
	
	def __permuteQPR(self, x : int) -> int:
		x = self.__overflow32(x)
		prime = 4294967291
		if x >= prime:
			return x
		residue = self.__overflow64(x * x) % prime
		if x <= int(prime / 2):
			return residue
		else:
			return prime - residue
	
	def __init__(self, seedBase: int, seedOffset: int):
		self.__m_index = self.__permuteQPR(self.__permuteQPR(seedBase) + 0x682f0161)
		self.__m_intermediateOffset = self.__permuteQPR(self.__permuteQPR(seedOffset) + 0x46790905)
	
	def next(self):
		i = self.__permuteQPR((self.__permuteQPR(self.__m_index) + self.__m_intermediateOffset) ^ 0x5bf03635)
		self.__m_index = self.__overflow32(self.__m_index + 1)
		return i



# code content generator
class LucaUserCode:

    version = b'\x03'
    deviceType = b'\x01' # 0: iOS; 1: Android; 2: static
    keyId = b'\x01'
    unixTimestampRoundedLE = [0, 0, 0, 0] # rounded to last full minute, encoded litle endian
    randomPayload = bytes(89) # simulate encrypted data

    __rBase = None # for random numbers

    def __init__(self):
        unixTimestampRounded = int( int(time.time() / 60) * 60 )
        self.__rBase = RandomSequenceOfUnique(unixTimestampRounded, unixTimestampRounded + 1)
        self.keyId = self.getRandomBytes(1)
        self.unixTimestampRoundedLE = unixTimestampRounded.to_bytes(4, byteorder='little')
        self.randomPayload = self.getRandomBytes(89)
    
    def getRandomBytes(self, length: int) -> bytes:
        randomContext = RandomSequenceOfUnique(self.__rBase.next(), self.__rBase.next())
        b = bytes()
        i = 0
        while(i < int(length/4)+1):
            b += randomContext.next().to_bytes(4, byteorder='big') # byteorder doesn't matter here but has to have a value
            i += 1
        return b[0:length]
    
    def getCodeBytesWithoutChecksum(self) -> bytes:
        return self.version + self.deviceType + self.keyId + self.unixTimestampRoundedLE + self.randomPayload
    
    def getChecksumBytes(self, payload: bytes) -> bytes:
        h = hashlib.sha256()
        h.update(payload)
        return h.digest()[0:4]
    
    def getCodeBytes(self) -> bytes:
        return self.getCodeBytesWithoutChecksum() + self.getChecksumBytes(self.getCodeBytesWithoutChecksum())
    
    def z85encode(self, rawbytes):
        """encode raw bytes into Z85"""
        # Z85CHARS is the base 85 symbol table
        Z85CHARS = b"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-:+=^!/*?&<>()[]{}@%$#"
        # Z85MAP maps integers in [0,84] to the appropriate character in Z85CHARS
        Z85MAP = dict([(c, idx) for idx, c in enumerate(Z85CHARS)])

        _85s = [85 ** i for i in range(5)][::-1]
        # Accepts only byte arrays bounded to 4 bytes
        if len(rawbytes) % 4:
            raise ValueError("length must be multiple of 4, not %i" % len(rawbytes))

        nvalues = len(rawbytes) / 4

        values = struct.unpack('>%dI' % nvalues, rawbytes)
        encoded = []
        for v in values:
            for offset in _85s:
                encoded.append(Z85CHARS[(v // offset) % 85])

        return bytes(encoded)

    def getCode(self) -> str:
        return self.z85encode(self.getCodeBytes()).decode('utf-8')



if __name__ == '__main__':
    c = LucaUserCode()
    # print('version        ', c.version.hex())
    # print('device type    ', c.deviceType.hex())
    # print('timestamp      ', c.unixTimestampRoundedLE.hex())
    # print('random payload ', c.randomPayload.hex())
    # print('code w/o check ', c.getCodeBytesWithoutChecksum().hex())
    # print('checksum       ', c.getChecksumBytes(c.getCodeBytesWithoutChecksum()).hex())
    # print('final code     ', c.getCodeBytes().hex())
    # print('encoded code   ', c.getCode())
    print(c.getCode())
