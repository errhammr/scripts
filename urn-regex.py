#!/usr/bin/env python3

import sys

import re

# json is just for pretty printing here
import json

# see [RFC8141](https://datatracker.ietf.org/doc/html/rfc8141) for details
# group #0 is the Namespace Identifier (NID)
# group #1 is the Namespace Specific String (NSS)
# group #2 is the optional r-component
# group #3 is the optional q-component
# group #4 is the optional f-component
urn_regex = r"^urn:([a-z0-9][a-z0-9-]{1,31}):((?:[-a-z0-9()+,.:=@;$_!*'&~\/]|%[0-9a-f]{2})+)(?:(?:\?\+)((?:(?!\?=)(?:[-a-z0-9()+,.:=@;$_!*'&~\/\?]|%[0-9a-f]{2}))*))?(?:(?:\?=)((?:(?!#).)*))?(?:(?:#)((?:[-a-z0-9()+,.:=@;$_!*'&~\/\?]|%[0-9a-f]{2})*))?$"

urns = []

if len(sys.argv) > 1:
    urns = sys.argv
    urns.pop(0)
else:
    urns = [
        "urn:uuid:0bd6562b-10d0-4c38-98c8-79f1601c1b29?+CustomResolver:issuer=example.com?=CustomProvider:query=openFile#somePart",
        "urn:example:animal:ferret:nose",
        "urn:oid:2.25.338737637373283876011090077584382310711?=type=document",
        "urn:example:r-q-component-wrong-order?=q-component?+r-component"
    ]

resultData = {}

for urn in urns:

    #print(urn)
    m = re.match(
        urn_regex,
        urn,
        re.I
    )
    if m == None:
        print(f'Error: not a URN: \'{urn}\'', file=sys.stderr)
    else:
        g = m.groups()
        resultData[urn] = {
            'nid': g[0],
            'nss': g[1],
            'rco': g[2],
            'qco': g[3],
            'fco': g[4]
        }

print(
    json.dumps(resultData, indent=2)
)
