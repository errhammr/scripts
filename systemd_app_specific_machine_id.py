#!/usr/bin/env python3

import hmac, uuid

def get_app_specific(machine_id: uuid.UUID, app_id: uuid.UUID) -> uuid.UUID:
    return uuid.UUID(
        bytes=hmac.digest(
            machine_id.bytes, 
            app_id.bytes, 
            'sha256')[:16], 
        version=4)

if __name__ == '__main__':
    machine_id = uuid.uuid4()
    app_id = uuid.uuid4()
    app_specific_machine_id = get_app_specific(machine_id, app_id)
    print('Random example:')
    print(f'Example machine id:              {machine_id}')
    print(f'Example app id:                  {app_id}')
    print(f'Derived app specific machine id: {app_specific_machine_id}')

    machine_id = uuid.UUID('b4315c25-5ed4-4769-a09a-83de9236658d')
    app_id = uuid.UUID('3e206b85-afe1-48c9-8a41-d9aa0aca99a4')
    app_specific_machine_id = get_app_specific(machine_id, app_id)
    expected = uuid.UUID('9bb33671-8812-4499-9cd1-49f4d6df6e41')
    print()
    print('Real world example:')
    print(f'Example machine id:              {machine_id}')
    print(f'Example app id:                  {app_id}')
    print(f'Derived app specific machine id: {app_specific_machine_id}')
    if app_specific_machine_id == expected:
        print('OK: calculated value matches expected value')
    else:
        print('ERROR: calculated value DOES NOT match expected value')
