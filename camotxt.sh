#!/bin/sh

# Surrounds text with random characters.  This can be useful if you plan to
# print out a text file and send it as a letter.
# Due to padding on every line the script does not support input text with lines
# longer than 60 characters.
#
# Tested on OpenBSD 6.6-current using ksh
# Package dependencies:
#   base64
#   ossp-uuid
#   A shell that supports 'echo -n' (like ksh, bash, among others)
#
# Usage:
#   cat file.txt | camotxt.sh [<number>]
#   <number> is the total number of lines in the output file.  However there are
#   exactly 5 lines prepended and at least 5 lines appended to the input text if
#   this number is too small.
#   The <number> defaults to 75 if none is given as command line argument.

dbg(){
	echo "$1" >&2
}

random_line(){
#	dbg "random_line()"
	echo "$(dd if=/dev/urandom ibs=54 count=1 2>/dev/null | base64 | tr -d '[:space:]')"
}

random_border(){
#	dbg "random_border()"
	dd if=/dev/urandom ibs=3 count=1 2>/dev/null | base64 | tr -d '[:space:]'
}

text_line(){
	LEN="${#1}"
	REMAINING=$(( 60 - LEN ))
	if [ "$LEN" -gt 60 ]; then
		echo "Error: line of text is longer than 60 chars" >&2
		exit 1
	fi
	echo -n "$(random_border)  $1"
	while [ "$REMAINING" -gt 0 ]; do
		echo -n " "
		REMAINING=$(( REMAINING - 1 ))
	done
	echo "  $(random_border)"
}

main(){
	COUNT=4
	while [ $COUNT -gt 0 ]; do
		random_line
		COUNT=$(( COUNT - 1 ))
	done
	text_line ""
	LINECOUNT=0
	while IFS= read -r line; do
		text_line "$line"
		LINECOUNT=$(( LINECOUNT + 1 ))
	done
	text_line ""
	COUNT=$(( ${1:-75} - 6 - LINECOUNT ))
	if [ $COUNT -lt 4 ]; then
		COUNT=4
	fi
	while [ $COUNT -gt 0 ]; do
		random_line
		COUNT=$(( COUNT - 1 ))
	done
}

main "$@"
