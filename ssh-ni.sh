#!/bin/sh

set -e

SSH_NI_KEYFILE="${1}"

if [ ! -f "${SSH_NI_KEYFILE}" ]; then
	cat >&2 <<EOF
Error: SSH pubkey file does not exist: '${1}'

Usage: ${0} SSH-PUBKEY-FILE

Example:
    \$ ${0} $HOME/.ssh/id_ed25519.pub
Example result:
    ni:///sha-256;ObHnHPH39xx2Jfslxl84DwdKGAuB3u8DToKldKaUPT0?hint=ssh-pubkey-fingerprint&ssh-key-type=ssh-ed25519
EOF
	exit 1
fi

echo "ni:///sha-256;$(cat "${SSH_NI_KEYFILE}" | cut -d ' ' -f 2 | base64 -d | sha256sum | cut -d ' ' -f 1 | xxd -r -p | base64 | tr '+/' '-_' | tr -d '=')?hint=ssh-pubkey-fingerprint&ssh-key-type=$(cat "${SSH_NI_KEYFILE}" | cut -d ' ' -f 1)"
