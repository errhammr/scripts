#!/bin/sh

NI_HOST=${1:?"Usage: $(basename "$0") <hostname> [<optional expected URI of certificate>]"}

TIMESTAMP_NOW="$(date -u +%Y%m%dT%H%M%SZ)"

CERT_URI_NI="$(python3 ~/dev/python3/uri-ni.py \
    --uri 'nih:1;'"$(openssl s_client -connect "${NI_HOST}:443" </dev/null 2>/dev/null | openssl x509 -noout -pubkey | openssl asn1parse -noout -inform pem -out - | openssl dgst -sha256 -hex - | cut -d '=' -f 2 | tr -d '[:space:]')" \
    --authority "${NI_HOST}" \
    --query 'hint=pubkey-fingerprint&timestamp='"$TIMESTAMP_NOW")"

echo "${CERT_URI_NI}"
python3 ~/dev/python3/uri-ni.py \
    --uri "${CERT_URI_NI}" \
    --nih
python3 ~/dev/python3/uri-ni.py \
    --uri "${CERT_URI_NI}" \
    --syncthing-id

if [ -n "${2}" ]; then
    python3 ~/dev/python3/uri-ni.py \
        --uri "${CERT_URI_NI}" \
        --verify "${2}"
fi
