#!/bin/sh

set -e

# Generates a certificates using the following configuration.
# 1)  Generates a CA certificate if it doesn't already exist
# 2)  Generates a sub CA certificate if it doesn't already exist
# 3)  Generates a client certificate if it doesn't already exist
#
# Adapt the configuration section to your needs.
# You probably want to change the CA and sub CA sections before the
# first run only and the client section before each run.
#
# The script assumes the CA and sub CA files to stay where they are.
# If it cannot find the CA or sub CA files, it will create new ones
# that will be different from the previous ones, which will break
# things!
#
# All private keys of all certificates are encrypted with individual
# passwords that are stored in plain text in individual password
# files.
# PROTECT THESE PASSWORD FILES!
#
# Created files:
# - *-ca.pass  .......  CA secret key password file
# - *-ca-key.pem  ....  CA secret key file
# - *-ca-crt.pem  ....  CA certificate
# - *-sub.pass  ......  Sub CA secret key password file
# - *-sub-key.pem  ...  Sub CA secret key file
# - *-sub-crt.pem  ...  Sub CA certificate
# - *-sub-chain.pem  .  Sub CA certificate chain
# - *-client.pass  ...  Client secret key password file
# - *-client-key.pem    Client secret key file
# - *-client-crt.pem    Client certificate
# - *-client-chain.pem  Client certificate chain
# - *-client-key.pfx    Client secret key and certificate in PKCS#12 format
# - some config files that were created to tell openssl what to do

# Dependencies:
# - ossp-uuid
# - openssl/libressl



### BEGIN CONFIGURATION

CA_FILENAME_BASE="example-ca"
CA_CN="EXAMPLE CA"
CA_DOMAIN="example.local"
CA_SERIAL="$(uuid -v 4 -F SIV)"
CA_KEY_ALGO="P-384" # has to be a curve name
CA_HASH_ALGO="sha384" # has to be a hash algo name

SUB_CA_FILENAME_BASE="example-sub-ca"
SUB_CA_CN="EXAMPLE SUB CA"
SUB_CA_SERIAL="$(uuid -v 4 -F SIV)"
SUB_CA_KEY_ALGO="P-384" # has to be a curve name
SUB_CA_HASH_ALGO="sha384" # has to be a hash algo name

CLIENT_FILENAME_BASE="example-client"
CLIENT_CN="client@example.local"
CLIENT_ALT_NAME="email:$CLIENT_CN" # "email:$CLIENT_CN" or "dns:$CLIENT_CN"
CLIENT_SERIAL="$(uuid -v 4 -F SIV)"
CLIENT_KEY_ALGO="P-384" # has to be a curve name
CLIENT_HASH_ALGO="sha384" # has to be a hash algo name
#CLIENT_KEY_USAGE="nonRepudiation,digitalSignature,keyEncipherment"
CLIENT_KEY_USAGE="nonRepudiation,digitalSignature,keyEncipherment"
#CLIENT_EXTENDED_KEY_USAGE="clientAuth,codeSigning,emailProtection"
CLIENT_EXTENDED_KEY_USAGE="clientAuth,emailProtection"

### END CONFIGURATION



CA_FILE_CONFIG="$CA_FILENAME_BASE-ca.conf"
CA_FILE_PASSWORD="$CA_FILENAME_BASE-ca.pass"
CA_FILE_SECRETKEY="$CA_FILENAME_BASE-ca-key.pem"
CA_FILE_CERT="$CA_FILENAME_BASE-ca-crt.pem"

SUB_CA_REQ_FILE_CONFIG="$SUB_CA_FILENAME_BASE-sub-req.conf"
SUB_CA_FILE_CONFIG="$SUB_CA_FILENAME_BASE-sub-crt.conf"
SUB_CA_FILE_PASSWORD="$SUB_CA_FILENAME_BASE-sub.pass"
SUB_CA_FILE_SECRETKEY="$SUB_CA_FILENAME_BASE-sub-key.pem"
SUB_CA_FILE_REQ="$SUB_CA_FILENAME_BASE-sub-csr.pem"
SUB_CA_FILE_CERT="$SUB_CA_FILENAME_BASE-sub-crt.pem"
SUB_CA_CHAIN="$SUB_CA_FILENAME_BASE-sub-chain.pem"

CLIENT_REQ_FILE_CONFIG="$CLIENT_FILENAME_BASE-client-req.conf"
CLIENT_FILE_CONFIG="$CLIENT_FILENAME_BASE-client-crt.conf"
CLIENT_FILE_PASSWORD="$CLIENT_FILENAME_BASE-client.pass"
CLIENT_FILE_SECRETKEY="$CLIENT_FILENAME_BASE-client-key.pem"
CLIENT_FILE_REQ="$CLIENT_FILENAME_BASE-client-csr.pem"
CLIENT_FILE_CERT="$CLIENT_FILENAME_BASE-client-crt.pem"
CLIENT_CHAIN="$CLIENT_FILENAME_BASE-client-chain.pem"
CLIENT_PKCS12="$CLIENT_FILENAME_BASE-client-key.pfx"


SCRIPT_NAME="$0"
output() {
    echo "$SCRIPT_NAME: $1" >&2
}



# ----- CA -----


# Generate configuration file

if [ -f "$CA_FILE_CONFIG" ]; then
    output "Configuration file already exists. Using that instead of overwriting."
else

    cat > "$CA_FILE_CONFIG" <<EOF
[ req ]
x509_extensions = v3_ca
distinguished_name = req_distinguished_name
prompt = no
[ req_distinguished_name ]
CN=$CA_CN
[ v3_ca ]
subjectKeyIdentifier=hash
basicConstraints=critical,CA:true,pathlen:1
keyUsage=critical,keyCertSign,cRLSign
nameConstraints=critical,@nc
[ nc ]
permitted;otherName=1.3.6.1.5.5.7.8.7;IA5:$CA_DOMAIN
permitted;email.0=$CA_DOMAIN
permitted;email.1=.$CA_DOMAIN
permitted;DNS=$CA_DOMAIN
permitted;URI.0=$CA_DOMAIN
permitted;URI.1=.$CA_DOMAIN
permitted;IP.0=0.0.0.0/255.255.255.255
permitted;IP.1=::/ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff
EOF

fi


# Generate password file for private key encryption

if [ -f "$CA_FILE_PASSWORD" ]; then
    output "CA password file already exists. Using that instead of overwriting."
else

    uuid -v 4 > "$CA_FILE_PASSWORD"

fi


# Generate private key

if [ -f "$CA_FILE_SECRETKEY" ]; then
    output "CA secret key file already exists. Using that instead of overwriting."
else

    openssl genpkey -out "$CA_FILE_SECRETKEY" -algorithm EC -pkeyopt "ec_paramgen_curve:$CA_KEY_ALGO" -aes256 -pass "file:$CA_FILE_PASSWORD"

fi


# Generate CA cert

if [ -f "$CA_FILE_CERT" ]; then
    output "CA certificate file already exists. Using that instead of overwriting."
else

    openssl req -new "-$CA_HASH_ALGO" -x509 -set_serial "$CA_SERIAL" -days 1000000 -config "$CA_FILE_CONFIG" -key "$CA_FILE_SECRETKEY" -out "$CA_FILE_CERT" -passin "file:$CA_FILE_PASSWORD"
    
    # show cert fingerprints
    output "CA certificate fingerprints:"
    openssl x509 -in "$CA_FILE_CERT" -noout -fingerprint -sha1
    openssl x509 -in "$CA_FILE_CERT" -noout -fingerprint -sha256
    
    # show cert
    openssl x509 -in "$CA_FILE_CERT" -text -noout|less

fi



# ----- SUB CA -----


# Generate sub CA certificate request configuration file

if [ -f "$SUB_CA_REQ_FILE_CONFIG" ]; then
    output "Sub CA req configuration file already exists. Using that instead of overwriting."
else

    cat > "$SUB_CA_REQ_FILE_CONFIG" <<EOF
[ req ]
distinguished_name = req_distinguished_name
prompt = no
[ req_distinguished_name ]
CN=$SUB_CA_CN
EOF

fi


# Generate sub CA certificate configuration file

if [ -f "$SUB_CA_FILE_CONFIG" ]; then
    output "Sub CA configuration file already exists. Using that instead of overwriting."
else

    cat > "$SUB_CA_FILE_CONFIG" <<EOF
basicConstraints = critical, CA:true, pathlen:0
keyUsage=critical, keyCertSign
EOF

fi


# Generate password file for private key encryption

if [ -f "$SUB_CA_FILE_PASSWORD" ]; then
    output "Sub CA password file already exists. Using that instead of overwriting."
else

    uuid -v 4 > "$SUB_CA_FILE_PASSWORD"

fi


# Generate private key

if [ -f "$SUB_CA_FILE_SECRETKEY" ]; then
    output "Sub CA secret key file already exists. Using that instead of overwriting."
else

    openssl genpkey -out "$SUB_CA_FILE_SECRETKEY" -algorithm EC -pkeyopt "ec_paramgen_curve:$SUB_CA_KEY_ALGO" -aes256 -pass "file:$SUB_CA_FILE_PASSWORD"

fi


# Generate sub CA certificate request

if [ -f "$SUB_CA_FILE_REQ" ]; then
    output "Sub CA certificate request file already exists. Using that instead of overwriting."
else

    openssl req -new "-$SUB_CA_HASH_ALGO" -config "$SUB_CA_REQ_FILE_CONFIG" -key "$SUB_CA_FILE_SECRETKEY" -out "$SUB_CA_FILE_REQ" -passin "file:$SUB_CA_FILE_PASSWORD" -nodes

fi


# Generate sub CA certificate

if [ -f "$SUB_CA_FILE_CERT" ]; then
    output "Sub CA certificate file already exists. Using that instead of overwriting."
else

    openssl x509 "-$CA_HASH_ALGO" -CA "$CA_FILE_CERT" -CAkey "$CA_FILE_SECRETKEY" -set_serial "$SUB_CA_SERIAL" -days "$(( 356 * 5 ))" -req -in "$SUB_CA_FILE_REQ" -extfile "$SUB_CA_FILE_CONFIG" -out "$SUB_CA_FILE_CERT" -passin "file:$CA_FILE_PASSWORD"
    
    # show cert fingerprints
    output "Sub CA certificate fingerprints:"
    openssl x509 -in "$SUB_CA_FILE_CERT" -noout -fingerprint -sha1
    openssl x509 -in "$SUB_CA_FILE_CERT" -noout -fingerprint -sha256
    
    # show cert
    openssl x509 -in "$SUB_CA_FILE_CERT" -text -noout|less

fi


# Create certificate chain file

cat "$SUB_CA_FILE_CERT" "$CA_FILE_CERT" > "$SUB_CA_CHAIN"



# ----- CLIENT -----


# Generate client certificate request configuration file

if [ -f "$CLIENT_REQ_FILE_CONFIG" ]; then
    output "Client req configuration file already exists. Using that instead of overwriting."
else

    cat > "$CLIENT_REQ_FILE_CONFIG" <<EOF
[ req ]
distinguished_name = req_distinguished_name
prompt = no
[ req_distinguished_name ]
CN=$CLIENT_CN
EOF

fi


# Generate client certificate configuration file

if [ -f "$CLIENT_FILE_CONFIG" ]; then
    output "Client configuration file already exists. Using that instead of overwriting."
else

    cat > "$CLIENT_FILE_CONFIG" <<EOF
basicConstraints = critical,CA:false
keyUsage=critical,$CLIENT_KEY_USAGE
extendedKeyUsage=$CLIENT_EXTENDED_KEY_USAGE
subjectAltName=$CLIENT_ALT_NAME
EOF

fi


# Generate password file for private key encryption

if [ -f "$CLIENT_FILE_PASSWORD" ]; then
    output "Client password file already exists. Using that instead of overwriting."
else

    uuid -v 4 > "$CLIENT_FILE_PASSWORD"

fi


# Generate private key

if [ -f "$CLIENT_FILE_SECRETKEY" ]; then
    output "Client secret key file already exists. Using that instead of overwriting."
else

    openssl genpkey -out "$CLIENT_FILE_SECRETKEY" -algorithm EC -pkeyopt "ec_paramgen_curve:$CLIENT_KEY_ALGO" -aes256 -pass "file:$CLIENT_FILE_PASSWORD"

fi


# Generate client certificate request

if [ -f "$CLIENT_FILE_REQ" ]; then
    output "Client certificate request file already exists. Using that instead of overwriting."
else

    openssl req -new "-$CLIENT_HASH_ALGO" -config "$CLIENT_REQ_FILE_CONFIG" -key "$CLIENT_FILE_SECRETKEY" -out "$CLIENT_FILE_REQ" -passin "file:$CLIENT_FILE_PASSWORD" -nodes

fi


# Generate client certificate

if [ -f "$CLIENT_FILE_CERT" ]; then
    output "Client certificate file already exists. Using that instead of overwriting."
else

    openssl x509 "-$SUB_CA_HASH_ALGO" -CA "$SUB_CA_FILE_CERT" -CAkey "$SUB_CA_FILE_SECRETKEY" -set_serial "$CLIENT_SERIAL" -days "$(( 356 * 1 ))" -req -in "$CLIENT_FILE_REQ" -extfile "$CLIENT_FILE_CONFIG" -out "$CLIENT_FILE_CERT" -passin "file:$SUB_CA_FILE_PASSWORD"
    
    # show cert fingerprints
    output "Client certificate fingerprints:"
    openssl x509 -in "$CLIENT_FILE_CERT" -noout -fingerprint -sha1
    openssl x509 -in "$CLIENT_FILE_CERT" -noout -fingerprint -sha256
    
    # show cert
    openssl x509 -in "$CLIENT_FILE_CERT" -text -noout|less

fi


# Create certificate chain file

cat "$CLIENT_FILE_CERT" "$SUB_CA_FILE_CERT" "$CA_FILE_CERT" > "$CLIENT_CHAIN"


# Create a PKCS#12 file for additional convenience
# openssl doesn't like the input and output files to be the same file
# so a temporary copy of the password file is created and immediately
# deleted once it isn't needed anymore
cp "$CLIENT_FILE_PASSWORD" "$CLIENT_FILE_PASSWORD.2"
openssl pkcs12 -export -out "$CLIENT_PKCS12" -inkey "$CLIENT_FILE_SECRETKEY" -in "$CLIENT_FILE_CERT" -certfile "$SUB_CA_CHAIN" -passin "file:$CLIENT_FILE_PASSWORD" -password "file:$CLIENT_FILE_PASSWORD.2"
rm "$CLIENT_FILE_PASSWORD.2"



# ----- CLEANUP -----

output "Cleaning up..."
rm -v "$SUB_CA_FILE_REQ" "$CLIENT_FILE_REQ"



output "Done."
