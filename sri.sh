#!/bin/sh

if [ ! "sha256" = "${1:-"sha384"}" ] && [ ! "sha384" = "${1:-"sha384"}" ] && [ ! "sha512" = "${1:-"sha384"}" ]; then
    echo "Error: algorithm can only be sha256, sha384 or sha512 and not '${1}'" >&2
    exit 1
fi

printf "%s-" "${1:-"sha384"}"
cat "${2:-"-"}" | openssl dgst "-${1:-"sha384"}" -binary | openssl enc -base64 -A
echo
