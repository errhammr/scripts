# camotxt.sh

Surrounds text with random characters.  This can be useful if you plan to
print out a text file and send it as a letter.

Due to padding on every line the script does not support input text with lines
longer than 60 characters.

Tested on OpenBSD 6.6-current using ksh

**Package dependencies:**
* base64
* ossp-uuid
* A shell that supports 'echo -n' (like ksh, bash, among others)

**Usage:**

`cat file.txt | camotxt.sh [<number>]`

`<number>` is the total number of lines in the output file.  However there are
exactly 5 lines prepended and at least 5 lines appended to the input text if
this number is too small.
The `<number>` defaults to 75 if none is given as command line argument.

## Example

```
$ echo "Hello World" | sh camotxt.sh 15 
34SeuFelQ3MldWvCGTddq47qU6pFh243oJzTrX6NXR996g4LmPi2pt31d9DUGuBpXyDbYMDm
XyBz7a6dbREbuKNWc0dzc8FrUEwVipBSM9yR4/vqcxSjcSYHGVlFJ0KByIvMFWNDK16AcLtw
iN9v1sz9hHS6mmn3ck+dAv/lGhNUNyn5Y+C8705G7EyHXOJ6cGE5tvB6ka5pogxi6CNXSl5r
smhNNgDYl1BGg8K/8bJ71tj9ohf5HN2y0f0LBAoa/Ra3dBMN69TbI5e8bK1b6za58OSuM1RY
NAy3                                                                W2FE
ITNQ  Hello World                                                   +w2K
Gagv                                                                3nRo
DObhX5S+9eiozJkvEWgBpQ6hUW/fHhZtb1piUsV5SS6b2559oiUbckIeaHr/okPfj7ZTOowm
TTNqjJbysONK3cttY6S6UrnAaDRKMPb2ItIU3ypSHdVvKFEEBrgjrAhuICswUX9ZRCBfOQAk
JK9UO3SdPu2zuJ8Y76wDHn+9FmvG1JocMb34aMZozJ6/2qZRJO6dQa8iVjZYISdS4n0kP4TU
2enrKUMdQWIUBMObP5SjdhG7/EkGyuyJv3mtObx4TC1AhZ/cnysdqCLVgqZgrp/F3nKkXZQp
xH47OQM3t2oFJZZ0HdL5zCo1XSxSpkAh961n5/RyUXVKkFf+RgxxChNBAAhP8mMkX6f+baxh
KqQwRiHYU4i6/SHHnmEo6rkNRVf/XOUgDmRvFa+ZQJhK2NIIbG7fVfoawBumeERJUm2RFpW5
jlmqV+fFyGet1Fkru4Fv7hKtR/D7nf60x6gkPtYHoNCrE4smsj5WAk6kYAn/S+ugKM37jloa
5Qp9t22AnwfoL9mlj5yypSjny0F8wEcK4/odsvZXrxE7FTlGJWUxcnjkOQNiK3rybhjaboWp
$
```
