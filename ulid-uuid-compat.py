#!/usr/bin/env python3



# Creates a layer of compatibility between ULID and UUID.
#
# A UUIDv4 is essentially a bunch of random bytes with a few bits set to
# specific values. The UUIDv4 doesn't care about what the random bits and
# bytes it contain.
#
# A ULID starts with a 6 byte timestamp followed by 10 random bytes. The
# ULID doesn't care about what the random bytes contain.
#
# It just so happens that the UUID's first predefined bits start in byte
# number 7 so that the ULID's 6 byte timestamp can fit right before that.
# The remaining 10 bytes can be filled with the last 10 bytes from a
# UUIDv4.
#
# So no matter whether your system expects a UUIDv4 or a ULID, you can
# use a mixture of both - just like this script provides an
# implementation for - to maximise the compatibility of your identifiers
# across systems.
#
# Potential benefits:
#  - they are k-sortable because of the timestamp
#  - they contain 72 bits of random data and are therefore hard to guess
#
# Potential drawbacks:
#  - the timestamp can be extracted and analyzed by anyone



# needs `ulid` from `pip install ulid-py`
import uuid, ulid

def getUlid():
	# Generate a ULID using the last 10 bytes of a UUIDv4 as randomness
	return ulid.from_randomness(uuid.uuid4().bytes[6:])

def getUuid():
	# Convert a UUIDv4 compatible ULID to a UUIDv4
	return uuid.UUID(bytes=getUlid().bytes)



if __name__ == '__main__':
	the_ulid = getUlid()
	the_uuid = uuid.UUID(bytes=the_ulid.bytes)
	print('ULID: ', the_ulid)
	print('UUID: ', the_uuid)
