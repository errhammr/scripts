#!/bin/sh

set -e

BRK_DEBUG=0
if [ "$1" = "--debug" ]; then
    BRK_DEBUG=1
    echo "$2" | grep -i '^\([0-9A-F][0-9A-F]\)*$' >/dev/null 2>&1 && BRK="$2"
elif echo "$1" | grep -i '^\([0-9A-F][0-9A-F]\)*$' >/dev/null 2>&1; then
    BRK="$1"
    [ "$2" = "--debug" ] && BRK_DEBUG=1
fi

BRK_OUTPUT_TXT=0
if [ "$1" = "--txt" ] || [ "$2" = "--txt" ] || [ "$3" = "--txt" ]; then
    BRK_OUTPUT_TXT=1
fi
    

# check prerequisites
command -v xxd >/dev/null 2>&1 && BRK_HAVE_XXD=1 || BRK_HAVE_XXD=0
command -v uuid >/dev/null 2>&1 && BRK_HAVE_UUID=1 || BRK_HAVE_UUID=0

BRK_HAVE_PREREQUISITES=1
#need xxd only if no key material has been provided via the CLI
if [ -z "$BRK" ] && [ "$BRK_HAVE_XXD" -eq 0 ]; then
    echo "Error: missing command: xxd" >&2
    BRK_HAVE_PREREQUISITES=0
fi
#need uuid only in text output mode
if [ "$BRK_OUTPUT_TXT" -eq 1 ] && [ "$BRK_HAVE_UUID" -eq 0 ]; then
    echo "Error: missing command: uuid" >&2
    BRK_HAVE_PREREQUISITES=0
fi

if [ "$BRK_HAVE_PREREQUISITES" -eq 0 ]; then
    echo "Aborting because prerequisites check failed." >&2
    exit 1
elif [ "$BRK_DEBUG" -eq 1 ]; then
    echo "Debug: prerequisites check OK" >&2
fi

# main

hexToNumber() (
    printf "%06d" $(( 0x$1 * 11 ))
)

convertKeyToNum() (
    BRK_IS_FIRST=1
    echo "$1" | fold -w4 | while read -r piece16; do
        if [ $BRK_IS_FIRST -eq 1 ]; then
            BRK_IS_FIRST=0
            printf "%s" "$(hexToNumber "$piece16")"
        else
            printf "%s" "-$(hexToNumber "$piece16")"
        fi
    done
)

[ -z "$BRK" ] && BRK="$(xxd -l16 -ps /dev/urandom)"

if [ "$(( ${#BRK} % 4 ))" -ne 0 ]; then
    echo "Error: key material is not a multiple of 2 bytes (16 bits) in length! Aborting." >&2
    exit 2
fi

[ "$BRK_OUTPUT_TXT" -eq 0 ] && echo "$BRK"

BRK_NUM="$(convertKeyToNum "$BRK")"

[ "$BRK_OUTPUT_TXT" -eq 0 ] && echo "$BRK_NUM"

if [ "$BRK_OUTPUT_TXT" -eq 1 ]; then
BRK_UUID="$(uuid -v4 | tr '[:lower:]' '[:upper:]')"
cat <<EOF
BitLocker Drive Encryption recovery key

To verify that this is the correct recovery key, compare the start of the following identifier with the identifier value displayed on your PC.

Identifier:

        $BRK_UUID

If the above identifier matches the one displayed on your PC, then use the following key to unlock your drive.

Recovery Key:

        $BRK_NUM

If the above identifier doesn't match the one displayed by your PC, then this isn't the right key to unlock your drive.
Try another recovery key, or refer to http://go.microsoft.com/fwlink/?LinkID=260589 for additional assistance.
EOF
fi