#!/bin/sh

set -e

REDIRECT_SERVICE_URL="${REDIRECT_SERVICE_URL:-"https://0x0.st"}" # alternatively https://envs.sh
                                                                 # or any other instance compatible with https://git.0x0.st/mia/0x0

if [ -z "${1}" ]; then
	echo "Usage: ${0} <URL>" >&2
	exit 1
fi

DEBUG_01J1_GD5V_4=0

if [ "${2}" = "--debug" ]; then
        DEBUG_01J1_GD5V_4=1
fi

TEMP_HTML_DIR="$(mktemp -d -t 'redirect.XXXXXXXXXX')"
TEMP_HTML_FILE="${TEMP_HTML_DIR}/redirect.xhtml"

[ "${DEBUG_01J1_GD5V_4}" -eq 1 ] && echo "${0}: [DBG] Created temporary directory ${TEMP_HTML_DIR}" >&2

cat >"$TEMP_HTML_FILE" <<EOF
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
        <head> 
                <meta http-equiv="Refresh" content="0; url='${1}'" />
                <title>Redirect to ${1}</title>
        </head>
        <body> 
                <p> 
                        This page redirects you to
                        <a href="${1}">${1}</a>.
                        Please click the link if you are not redirected automatically.
                </p>
        </body>
</html>
EOF

[ "${DEBUG_01J1_GD5V_4}" -eq 1 ] && echo "${0}: [DBG] Wrote XHTML to file ${TEMP_HTML_FILE}" >&2

curl -s -F"file=@${TEMP_HTML_FILE}" "${REDIRECT_SERVICE_URL}"

if [ "${DEBUG_01J1_GD5V_4}" -eq 1 ]; then
        rm -r -v "${TEMP_HTML_DIR}" >&2
else
        rm -r "${TEMP_HTML_DIR}"
fi
