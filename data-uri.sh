#!/bin/sh

data_uri() (
	if [ ! -f "$1" ]; then
		echo "Usage: $0 <filename>" >&2
		exit 1
	fi
	DATA_URI_MIME="$(file --mime-type --brief "$1")"
	printf "data:%s;base64," "$DATA_URI_MIME"
	base64 -w0 "$1"
)
