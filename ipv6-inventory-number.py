#!/usr/bin/env python3

# one liner:
# python3 -c 'from ipaddress import IPv6Network;import sys;print(IPv6Network(sys.argv[1])[int(sys.argv[2])])' fd7a:f0f8:4d72::/64 42

# as script:
# ipv6-inventory-number.py fd7a:f0f8:4d72::/64 42

from ipaddress import IPv6Network
import sys

try:
    network = IPv6Network(sys.argv[1])
    address = network[int(sys.argv[2])]

    print(address.compressed) # compact output
    # print(address.exploded) # non-compact output
except BaseException as e:
    print(e, file=sys.stderr)
    print('Usage:    ipv6-inventory-number.py IPv6_NETWORK SERIAL', file=sys.stderr)
    print('Example:  ipv6-inventory-number.py fd7a:f0f8:4d72::/64 42', file=sys.stderr)
