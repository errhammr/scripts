#!/usr/bin/env perl

# This is my very first Perl script. Don't judge me!

use strict;
use warnings;

use Digest::SHA;


my ($filename, $alg) = @ARGV;
 
if (not defined $filename) {
  die "Usage: uri-ni.pl FILENAME [ALGO_BITS]\n";
}

if (not defined $alg) {
  $alg = '256';
}

my $algString = 'unknown';

$algString = 'sha-256' if $alg == '256';
$algString = 'sha-384' if $alg == '384';
$algString = 'sha-512' if $alg == '512';

die "Unknown algorithm '$alg'\n" if $algString eq 'unknown';


my $hasher = Digest::SHA->new($alg);

$hasher->addfile($filename, 'b');

my $dig = $hasher->b64digest; # conveniently doesn't output padding

$dig =~ tr/\+\//-_/; # convert base64 to url safe base64

print "ni:///$algString;$dig\n";
