<?php

declare(strict_types=1);

/**
 * Creates simple math problems in German using integers from 0 to 20
 */
class CaptchaQuestion
{
	/**
	 * German words for the numbers
	 */
	const numbersGerman = [
		0 => 'null',
		1 => 'eins',
		2 => 'zwei',
		3 => 'drei',
		4 => 'vier',
		5 => 'fünf',
		6 => 'sechs',
		7 => 'sieben',
		8 => 'acht',
		9 => 'neun',
		10 => 'zehn',
		11 => 'elf',
		12 => 'zwölf',
		13 => 'dreizehn',
		14 => 'vierzehn',
		15 => 'fünfzehn',
		16 => 'sechzehn',
		17 => 'siebzehn',
		18 => 'achtzehn',
		19 => 'neunzehn',
		20 => 'zwanzig',
	];

	/**
	 * German words for the operators
	 */
	const operationsGerman = [
		'+' => 'plus',
		'-' => 'minus',
		'*' => 'mal',
		'/' => 'geteilt durch',
	];

	protected string $question;
	protected string $answer;

	/**
	 * Returns a question and answer with a random operator.
	 * IF IN DOUBT, USE THIS FUNCTION
	 */
	public static function GetRandomQuestion() : CaptchaQuestion {
		switch(random_int(1, 4)) {
			case 1:
				return self::GetAdditionQuestion();
			break;
			case 2:
				return self::GetSubtractionQuestion();
			break;
			case 3:
				return self::GetMultiplicationQuestion();
			break;
			case 4:
				return self::GetDivisionQuestion();
			break;
			default:
				throw new RuntimeException('Unknown operation');
		}
	}

	/**
	 * Returns a question and answer with addition (+)
	 */
	public static function GetAdditionQuestion() : CaptchaQuestion {
		$m = random_int(0, 10);
		$n = random_int(0, 10);
		$r = $m + $n;
		return new CaptchaQuestion(self::FormulateMathGerman($m, $n, '+'), $r.'');
	}

	/**
	 * Returns a question and answer with substraction (-)
	 */
	public static function GetSubtractionQuestion() : CaptchaQuestion {
		$m = random_int(0, 10);
		$n = random_int(0, 10);
		$r = $m + $n;
		return new CaptchaQuestion(self::FormulateMathGerman($r, $n, '-'), $m.'');
	}

	/**
	 * Returns a question and answer with multiplication (*)
	 */
	public static function GetMultiplicationQuestion() : CaptchaQuestion {
		$m = random_int(1, 10);
		$n = random_int(1, intval(20 / $m));
		$r = $m * $n;
		if(random_int(0, 1) == 1) {
			// randomly swap operands
			$t = $m;
			$m = $n;
			$n = $t;
		}
		return new CaptchaQuestion(self::FormulateMathGerman($m, $n, '*'), $r.'');
	}

	/**
	 * Returns a question and answer with division (/)
	 */
	public static function GetDivisionQuestion() : CaptchaQuestion {
		$m = random_int(1, 10);
		$n = random_int(1, intval(20 / $m));
		$r = $m * $n;
		if(random_int(0, 1) == 1) {
			// randomly swap operands
			$t = $m;
			$m = $n;
			$n = $t;
		}
		return new CaptchaQuestion(self::FormulateMathGerman($r, $n, '/'), $m.'');
	}

	/**
	 * Returns a question as a German string. The operator and numbers
	 * are converted to German words
	 */
	public static function FormulateMathGerman(int $m, int $n, string $op) : string {
		return 'Was ergibt '.self::numbersGerman[$m].' '.self::operationsGerman[$op].' '.self::numbersGerman[$n].'?';
	}

	function __construct(string $q, string $a) {
		$this->question = $q;
		$this->answer = $a;
	}

	public function getQuestion() : string {
		return $this->question;
	}

	public function getAnswer() : string {
		return $this->answer;
	}
}