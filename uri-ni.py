#!/usr/bin/env python3

import base64, hashlib, re

try:
    import blake3
except ModuleNotFoundError:
    pass

class UriNi:

    NO_PREFERENCE = 0
    UPPER = 1
    LOWER = 2

    __rawHash = b''
    __authority = ''
    __query = ''
    __shortPrefixes = {
        '1': 32,
        '2': 16,
        '3': 15,
        '4': 12,
        '5':  8,
        '6':  4,
        # RFC 6920 section 9.4 -> https://datatracker.ietf.org/doc/html/rfc6920#section-9.4
        'sha-256'    : '1',
        'sha-256-128': '2',
        'sha-256-120': '3',
        'sha-256-96' : '4',
        'sha-256-64' : '5',
        'sha-256-32' : '6',
        # IANA registry for Named Information -> https://www.iana.org/assignments/named-information/named-information.xhtml#hash-alg
        'sha-384': '7',
        'sha-512': '8',
        'sha3-224': '9',
        'sha3-256': '10',
        'sha3-384': '11',
        'sha3-512': '12',
        'blake2s-256': None,
        'blake2b-256': None,
        'blake2b-512': None,
        # inofficial
        'blake3-256': None
    }
    _hexLookup = {
        'a': 10,
        'b': 11,
        'c': 12,
        'd': 13,
        'e': 14,
        'f': 15
    }
    _niRegex = r"^ni://([A-Za-z0-9\.-]*)/(?:([0-9]+|([a-z0-9]+-[0-9]+)(?:|-([0-9]+))));([A-Za-z0-9-_]+)(?:|\?(.*))$"
    _nihRegex = r"^nih:(?:([0-9]+|([a-z0-9]+-[0-9]+)(?:|-([0-9]+))));([a-f0-9-]+)(?:|;([a-f0-9]))(?:|\?(.*))$"
    _nihB32Regex = r"^nih-b32:(?:([0-9]+|([A-Za-z]+-[0-9]+)(?:|-([0-9]+))));([ABCDEFGHIJKLMNOPQRSTUVWXYZ234567-]+)(?:|;([ABCDEFGHIJKLMNOPQRSTUVWXYZ234567]))$"
    _nihB32CRegex = r"^nih-b32c:(?:([0-9]+|([A-Za-z]+-[0-9]+)(?:|-([0-9]+))));([0123456789ABCDEFGHJKMNPQRSTVWXYZ-]+)(?:|;([0123456789ABCDEFGHJKMNPQRSTVWXYZ]))$"
    _syncthingIdRegex = r"^([ABCDEFGHIJKLMNOPQRSTUVWXYZ234567]{7})-([ABCDEFGHIJKLMNOPQRSTUVWXYZ234567]{7})-([ABCDEFGHIJKLMNOPQRSTUVWXYZ234567]{7})-([ABCDEFGHIJKLMNOPQRSTUVWXYZ234567]{7})-([ABCDEFGHIJKLMNOPQRSTUVWXYZ234567]{7})-([ABCDEFGHIJKLMNOPQRSTUVWXYZ234567]{7})-([ABCDEFGHIJKLMNOPQRSTUVWXYZ234567]{7})-([ABCDEFGHIJKLMNOPQRSTUVWXYZ234567]{7})$"
    _b32alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'

    _ni_algo_to_hashlib = {
        'sha-256': 'sha256',
        'sha-384': 'sha384',
        'sha-512': 'sha512',
        'sha3-224': 'sha3_224',
        'sha3-256': 'sha3_256',
        'sha3-384': 'sha3_384',
        'sha3-512': 'sha3_512',
        'blake2s-256': 'blake2s',
        #'blake2b-256': 'blake2b', # this script doesn't handle the initialization of the digest size properly
        'blake2b-512': 'blake2b',
        'blake3-256': 'blake3', # this script doesn't use hashlib for blake3
    }

    _ni_algo_id_to_name_base = {
        '1': 'sha-256',
        '2': 'sha-256',
        '3': 'sha-256',
        '4': 'sha-256',
        '5': 'sha-256',
        '6': 'sha-256',
        '7': 'sha-384',
        '8': 'sha-512',
        '9': 'sha3-224',
        '10': 'sha3-256',
        '11': 'sha3-384',
        '12': 'sha3-512'
    }

    __hash_algo_base = None

    def __init__(self, algo: str = 'sha-256'):
        if algo in self._ni_algo_to_hashlib:
            self.__hash_algo_base = algo
        else:
            raise ValueError('Unsupported hash algorithm: ' + str(algo))
        self.__hasher = None

    def ofString(string: str, algo: str = 'sha-256'):
        niObj = UriNi(algo)
        niObj._setStringUTF8(string)
        return niObj
    
    def ofFile(filename: str, algo: str = 'sha-256'):
        niObj = UriNi(algo)
        if filename == '-':
            niObj._setFileStdin()
        else:
            niObj._setFile(filename)
        return niObj
    
    def ofUri(uri: str):
        # try extracting ni:// URI
        niUriMatch = re.match(UriNi._niRegex, uri, re.IGNORECASE)
        if niUriMatch != None:
            # this is probably a ni:// URI
            groups = niUriMatch.groups()
            print(groups)
            authority = groups[0]
            algo = groups[1].lower()
            plainAlgo = groups[2].lower()  if groups[2] is not None else None
            algoBits = groups[3]
            encodedHash = groups[4]
            query = groups[5]
            if not ((plainAlgo in UriNi._ni_algo_to_hashlib) or (algo in UriNi._ni_algo_id_to_name_base)):
                raise ValueError('Unknown algorithm in URI.')
            if algo in UriNi._ni_algo_id_to_name_base:
                plainAlgo = UriNi._ni_algo_id_to_name_base[algo]
            while len(encodedHash) % 4 != 0:
                encodedHash = encodedHash + '='
            rawHash = base64.urlsafe_b64decode(encodedHash)
            niObj = UriNi(plainAlgo)
            niObj._setRawHashValue(rawHash)
            niObj.setAuthority(authority)
            if query != None and query != '':
                niObj.setQuery(query)
            return niObj
        
        # try extracting nih: URI
        niUriMatch = re.match(UriNi._nihRegex, uri, re.IGNORECASE)
        if niUriMatch != None:
            # this is probably a nih: URI
            groups = niUriMatch.groups()
            algo = groups[0].lower()
            plainAlgo = groups[1].lower() if groups[1] is not None else None
            algoBits = groups[2]
            encodedHash = groups[3].replace('-', '').lower() if groups[3] is not None else None
            checkDigit = groups[4].lower()  if groups[4] is not None else None
            query = groups[5]
            if not ((plainAlgo in UriNi._ni_algo_to_hashlib) or (algo in UriNi._ni_algo_id_to_name_base)):
                raise ValueError('Unknown algorithm in URI.')
            if algo in UriNi._ni_algo_id_to_name_base:
                plainAlgo = UriNi._ni_algo_id_to_name_base[algo]
            if checkDigit != None and len(checkDigit) == 1:
                expectedCheckDigit = UriNi._hexLuhn(encodedHash)
                if expectedCheckDigit != checkDigit:
                    raise RuntimeError(f'Check digit mismatch: found {checkDigit} but expected {expectedCheckDigit} in {encodedHash};{checkDigit}')
            rawHash = bytes.fromhex(encodedHash)
            niObj = UriNi(plainAlgo)
            niObj._setRawHashValue(rawHash)
            if query != None and query != '':
                niObj.setQuery(query)
            return niObj
        
        # try extracting nih-b32: URI
        niUriMatch = re.match(UriNi._nihB32Regex, uri, re.IGNORECASE)
        if niUriMatch != None:
            # this is probably a nih: URI
            groups = niUriMatch.groups()
            algo = groups[0].lower()  if groups[0] is not None else None
            plainAlgo = groups[1].lower() if groups[1] is not None else None
            algoBits = groups[2]
            encodedHash = groups[3].replace('-', '').upper()
            checkDigit = groups[4].upper() if groups[4] is not None else None
            if not ((plainAlgo in UriNi._ni_algo_to_hashlib) or (algo in UriNi._ni_algo_id_to_name_base)):
                raise ValueError('Unknown algorithm in URI.')
            if algo in UriNi._ni_algo_id_to_name_base:
                plainAlgo = UriNi._ni_algo_id_to_name_base[algo]
            if checkDigit != None and len(checkDigit) == 1:
                expectedCheckDigit = UriNi.luhnModN(encodedHash, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567')
                if expectedCheckDigit != checkDigit:
                    raise RuntimeError(f'Check digit mismatch: found {checkDigit} but expected {expectedCheckDigit} in {encodedHash};{checkDigit}')
            rawHash = base64.b32decode(encodedHash + '='*((8-len(encodedHash)%8)%8))
            niObj = UriNi(plainAlgo)
            niObj._setRawHashValue(rawHash)
            return niObj

        # try extracting nih-b32c: URI
        niUriMatch = re.match(UriNi._nihB32CRegex, uri, re.IGNORECASE)
        if niUriMatch != None:
            # this is probably a nih: URI
            groups = niUriMatch.groups()
            algo = groups[0].lower() if groups[0] is not None else None
            plainAlgo = groups[1].lower() if groups[1] is not None else None
            algoBits = groups[2]
            encodedHash = groups[3].replace('-', '').upper().translate(str.maketrans('OIL', '011'))
            checkDigit = groups[4].upper().translate(str.maketrans('OIL', '011')) if groups[4] is not None else None
            if not ((plainAlgo in UriNi._ni_algo_to_hashlib) or (algo in UriNi._ni_algo_id_to_name_base)):
                raise ValueError('Unknown algorithm in URI.')
            if algo in UriNi._ni_algo_id_to_name_base:
                plainAlgo = UriNi._ni_algo_id_to_name_base[algo]
            if checkDigit != None and len(checkDigit) == 1:
                expectedCheckDigit = UriNi.luhnModN(encodedHash, '0123456789ABCDEFGHJKMNPQRSTVWXYZ')
                if expectedCheckDigit != checkDigit:
                    raise RuntimeError(f'Check digit mismatch: found {checkDigit} but expected {expectedCheckDigit} in {encodedHash};{checkDigit}')
            rawHash = base64.b32decode(encodedHash.translate(str.maketrans("0123456789ABCDEFGHJKMNPQRSTVWXYZ", "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567")) + '='*((8-len(encodedHash)%8)%8))
            niObj = UriNi(plainAlgo)
            niObj._setRawHashValue(rawHash)
            return niObj

        # try extracting syncthing ID
        niUriMatch = re.match(UriNi._syncthingIdRegex, uri)
        if niUriMatch != None:
            # this is probably a syncthing ID
            groups = niUriMatch.groups()
            parts = (
                (
                    groups[0] + groups[1][:-1],
                    groups[1][-1:]
                ),
                (
                    groups[2] + groups[3][:-1],
                    groups[3][-1:]
                ),
                (
                    groups[4] + groups[5][:-1],
                    groups[5][-1:]
                ),
                (
                    groups[6] + groups[7][:-1],
                    groups[7][-1:]
                )
            )
            plainB32ID = ''
            for part in parts:
                expectedCheckDigit = UriNi.luhnModN_syncthingVariant(part[0], UriNi._b32alphabet)
                if expectedCheckDigit != part[1]:
                    raise ValueError('Syncthing ID is invalid: check digit of group ' + '-'.join([part[0][i:i+7] for i in range(0, len(part[0]), 7)]) + ' is ' + part[1] + ' but ' + expectedCheckDigit + ' was expected.')
                else:
                    plainB32ID = plainB32ID + part[0]
            plainB32ID = plainB32ID + '===='
            niObj = UriNi()
            niObj._setRawHashValue(base64.b32decode(plainB32ID.encode('utf-8')))
            return niObj
        
        raise ValueError('This does not look like a valid ni: or nih: URI')

    
    def insertDashes(string: str, x: int) -> str:
        if x == 0:
            return string
        new = ''
        cnt = 0
        for ch in string:
            if cnt%x==0 and cnt!=0:
                new += '-'
            cnt += 1
            new += str(ch)
        return new

    def __getNewHasher(self):
        if self._ni_algo_to_hashlib[self.__hash_algo_base] == 'blake3':
            try:
                return blake3.blake3(max_threads=blake3.blake3.AUTO)
            except NameError:
                raise ValueError('Unable to create hasher for ' + self.__hash_algo_base + ': missing module blake3 (pip install blake3)')
        return hashlib.new(self._ni_algo_to_hashlib[self.__hash_algo_base])

    def _setStringUTF8(self, string: str):
        hasher = self.__getNewHasher()
        hasher.update(string.encode('utf8'))
        self.__rawHash = hasher.digest()

    def _setFile(self, filename: str):
        with open(filename,"rb") as f:
            hasher = self.__getNewHasher()
            # Read and update hash string value in blocks of 4K
            for byte_block in iter(lambda: f.read(4096 * 1024),b""):
                hasher.update(byte_block)
            self.__rawHash = hasher.digest()

    def _setFileStdin(self):
        hasher = self.__getNewHasher()
        # Read and update hash string value in blocks of 1K
        for byte_block in iter(lambda: sys.stdin.buffer.read(1024),b""):
            hasher.update(byte_block)
        self.__rawHash = hasher.digest()
    
    def _rawUpdate(self, rawData):
        if self.__hasher is None:
            self.__hasher = self.__getNewHasher()
        self.__hasher.update(rawData)
    
    def _rawFinish(self):
        if self.__hasher is None:
            self.__hasher = self.__getNewHasher()
        self.__rawHash = self.__hasher.digest()

    def _setRawHashValue(self, rawHash: bytes):
        self.__rawHash = rawHash

    def setAuthority(self, authority: str):
        self.__authority = authority

    def setQuery(self, query: str):
        self.__query = query

    def formatNi(self, numBytes: int = 0, casePreference: int = 0, preferShortAlgoId: bool = False) -> str:
        if numBytes == 0:
            numBytes = len(self.__rawHash)
        if numBytes > len(self.__rawHash):
            raise ValueError('numBytes cannot be greater than the hash value length of ' + str(len(self.__rawHash)))
        algo = self.__hash_algo_base
        if numBytes < ( int(algo.split('-')[-1]) / 8 ):
            algo = algo + '-' + str(numBytes * 8)
        hashStr = base64.urlsafe_b64encode(self.__rawHash[0:numBytes]).decode('utf8').replace('=', '')

        if preferShortAlgoId and algo in self.__shortPrefixes and self.__shortPrefixes[algo] is not None:
            algo = self.__shortPrefixes[algo]

        query = ''
        if self.__query != '':
            query = '?' + self.__query

        ni = 'ni://' + self.__authority + '/' + algo + ';'
        if casePreference == self.LOWER:
            ni = ni.lower()
        elif casePreference == self.UPPER:
            ni = ni.upper()
        ni = ni + hashStr + query
        return ni
    
    def formatNih(self, numBytes: int = 0, withCheckDigit: bool = True, dashEvery: int = 4, casePreference: int = 0) -> str:
        if numBytes == 0:
            numBytes = len(self.__rawHash)
        if numBytes > len(self.__rawHash):
            raise ValueError('numBytes cannot be greater than the hash value length of ' + str(len(self.__rawHash)))
        algo = self.__hash_algo_base
        if numBytes < ( int(algo.split('-')[-1]) / 8 ):
            algo = algo + '-' + str(numBytes * 8)
        hashStr = self.__rawHash[0:(numBytes)].hex()
        hashStrDashed = UriNi.insertDashes(hashStr, dashEvery)

        if algo in self.__shortPrefixes and self.__shortPrefixes[algo] is not None:
            algo = self.__shortPrefixes[algo]
        nih = 'nih:' + algo + ';' + hashStrDashed

        if withCheckDigit:
            luhn = UriNi._hexLuhn(hashStr)
            nih = nih + ';' + luhn

        if casePreference == self.LOWER:
            nih = nih.lower()
        elif casePreference == self.UPPER:
            nih = nih.upper()

        return nih

    def formatNihBase32(self, numBytes: int = 0, withCheckDigit: bool = True, dashEvery: int = 7, crockford: bool = False, casePreference: int = 0) -> str:
        if numBytes == 0:
            numBytes = len(self.__rawHash)
        if numBytes > len(self.__rawHash):
            raise ValueError('numBytes cannot be greater than the hash value length of ' + str(len(self.__rawHash)))
        algo = self.__hash_algo_base
        if numBytes < ( int(algo.split('-')[-1]) / 8 ):
            algo = algo + '-' + str(numBytes * 8)
        hashStr = base64.b32encode(self.__rawHash[0:(numBytes)]).decode('utf-8').strip('=')
        hashStrDashed = UriNi.insertDashes(hashStr, dashEvery)

        if algo in self.__shortPrefixes and self.__shortPrefixes[algo] is not None:
            algo = self.__shortPrefixes[algo]
        
        nih = None

        if not crockford:
            nih = 'nih-b32:' + algo + ';' + hashStrDashed
            if withCheckDigit:
                luhn = UriNi.luhnModN(hashStr, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567')
                nih = nih + ';' + luhn
        
        else:
            nih = nih = 'nih-b32c:' + algo + ';' + hashStrDashed.translate(str.maketrans('ABCDEFGHIJKLMNOPQRSTUVWXYZ234567', '0123456789ABCDEFGHJKMNPQRSTVWXYZ'))
            if withCheckDigit:
                luhn = UriNi.luhnModN(hashStr.translate(str.maketrans('ABCDEFGHIJKLMNOPQRSTUVWXYZ234567', '0123456789ABCDEFGHJKMNPQRSTVWXYZ')), '0123456789ABCDEFGHJKMNPQRSTVWXYZ')
                nih = nih + ';' + luhn

        if casePreference == self.LOWER:
            nih = nih.lower()
        elif casePreference == self.UPPER:
            nih = nih.upper()

        return nih

    def formatSyncthingId(self) -> str:
        if len(self.__rawHash) != 32:
            raise ValueError('Syncthing IDs can only be generated for hash values of length 32 bytes')
        b32s = base64.b32encode(self.__rawHash).decode('utf-8').strip('=')
        b32parts = [b32s[i:i+13] for i in range(0, len(b32s), 13)]
        b32alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'
        theId = ''.join([part + UriNi.luhnModN_syncthingVariant(part, b32alphabet) for part in b32parts])
        theIdDashed = '-'.join([theId[i:i+7] for i in range(0, len(theId), 7)])
        return theIdDashed

    def formatWellKnown(self, protocol: str = '', numBytes: int = 0, nonStandard: bool = False, filename: str = '') -> str:
        if numBytes == 0:
            numBytes = len(self.__rawHash)
        if numBytes > len(self.__rawHash):
            raise ValueError('numBytes cannot be greater than the hash value length of ' + str(len(self.__rawHash)))
        algo = self.__hash_algo_base
        if numBytes < ( int(algo.split('-')[-1]) / 8 ):
            algo = algo + '-' + str(numBytes * 8)
        hashStr = base64.urlsafe_b64encode(self.__rawHash[0:numBytes]).decode('utf8').replace('=', '')
        prefix = ''
        if protocol != None and protocol != '':
            prefix = protocol + '://'
        if self.__authority != '':
            prefix = prefix + self.__authority

        query = ''
        if self.__query != '':
            query = '?' + self.__query

        nonStdSuffix = ''
        if nonStandard:
            nonStdSuffix = '.d/' + filename

        ni = prefix + '/.well-known/ni/' + algo + '/' + hashStr + nonStdSuffix + query
        return ni

    def _hexLuhn(hexString: str) -> str:
        return UriNi.luhnModN(hexString, '0123456789abcdef')
    
    def luhnModN(s: str, alphabet: str) -> str:
        """
        returns the Luhn mod N check digit of a string and a given alphabet
        """
        count = 0
        luhnVal = 0
        multiplyOn = (len(s) + 1) % 2
        alphabetLength = len(alphabet)
        for char in s:
            i = alphabet.find(char)

            if count % 2 == multiplyOn:
                i = i * 2

            i = int(i / alphabetLength) + (i % alphabetLength)
            luhnVal = (luhnVal + i) % alphabetLength
            count = count + 1
        luhnVal = (alphabetLength - (luhnVal % alphabetLength)) % alphabetLength
        return alphabet[luhnVal]

    def luhnModN_syncthingVariant(s: str, alphabet: str) -> str:
        """
        returns the syncthing variant of the Luhn mod N check digit of a string and a given alphabet
        """
        count = 0
        luhnVal = 0
        multiplyOn = 1
        alphabetLength = len(alphabet)
        for char in s:
            i = alphabet.find(char)

            if count % 2 == multiplyOn:
                i = i * 2

            i = int(i / alphabetLength) + (i % alphabetLength)
            luhnVal = (luhnVal + i) % alphabetLength
            count = count + 1
        luhnVal = (alphabetLength - (luhnVal % alphabetLength)) % alphabetLength
        return alphabet[luhnVal]

def canonicalizeJson(d: dict, sortLists: bool) -> str:
    j = toCompactJSON(canonicalizeDict(d, sortLists))
    return j

def canonicalizeDict(d, sortLists: bool):
    if isinstance(d, dict):
        n = dict()
        for (key, value) in d.items():
            n[key] = canonicalizeDict(value, sortLists)
        return n
    elif isinstance(d, list):
        if sortLists:
            return sorted(d, key=lambda x: x.id if hasattr(x, 'id') else canonicalizeJson(x, sortLists))
        else:
            return d
    else:
        return d

def toCompactJSON(d: dict, sortKeys: bool = True) -> str:
    return json.dumps(d, sort_keys=sortKeys, separators=(',', ':'))

if __name__ == '__main__':

    import argparse, mimetypes, os, sys, json
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", help="create an URI from a file; mutually exclusive with -s and -u")
    parser.add_argument("-s", "--string", help="create an URI from a string; mutually exclusive with -f and -u")
    parser.add_argument("-u", "--uri", help="create an URI from an URI, e. g. for transforming it into another representation; mutually exclusive with -s and -f")
    parser.add_argument("-b", "--bytes", help="truncate the hash to this number of bytes; default 32")
    parser.add_argument("-a", "--authority", help="set authority for the URI")
    parser.add_argument("-q", "--query", help="set query part for the URI (without leading question mark)")
    parser.add_argument("-d", "--dashes", help="when generating a nih: URI, put dashes every nth char; default 4")
    parser.add_argument("-n", "--nocheck", help="when generating a nih: URI, do not include a check digit",
                        action="store_true")
    parser.add_argument("--nih", help="generate a nih: URI instead of a ni:// URI; mutually exclusive with -w",
                        action="store_true")
    parser.add_argument("--nih-b32", help="generate a non-standard nih-b32: URI instead of a ni:// URI; Base32 encoding",
                        action="store_true")
    parser.add_argument("--nih-b32c", help="generate a non-standard nih-b32c: URI instead of a ni:// URI; Crockford's Base32 encoding",
                        action="store_true")
    parser.add_argument("-w", "--wellknown", help="generate a .well-known URL instead of a ni:// URI; mutually exclusive with --nih",
                        action="store_true")
    parser.add_argument("--wknonstandard", help="generate a .well-known URL instead of a ni:// URI; deviates from RFC by placing the original filename inside a directory",
                        action="store_true")
    parser.add_argument("--syncthing-id", help="format the output as a Syncthing ID",
                        action="store_true")
    parser.add_argument("-p", "--protocol", help="when generating a .well-known URL, use this protocol, e. g. \"https\"; default: none")
    parser.add_argument("--json-integrity", help="add an 'integrity' field to the top level JSON object of a given JSON file")
    parser.add_argument("--json-sort-arrays", help="sort arrays before hashing. Only has an effect when --json-integrity is given.",
                        action="store_true")
    parser.add_argument("--json-integrity-key", help="use the given key for the integrity field or '-' to output the integrity value to stdout")
    parser.add_argument("-V", "--verify", help="verify that the given ni/nih URI contains the same hash value")
    parser.add_argument("--algo", help="when creating a new URI by hashing data, use the given algorithm. Supported algorithms: " + ', '.join(sorted(list(UriNi._ni_algo_to_hashlib.keys()))), default='sha-256')
    parser.add_argument("-U", "--upper", help="make all case insensitive parts upper case",
                        action="store_true")
    parser.add_argument("-l", "--lower", help="make all case insensitive parts lower case",
                        action="store_true")
    parser.add_argument("-z", "--short", help="prefer short algorithm IDs",
                        action="store_true")
    parser.add_argument("--remote", help="hash the file behind a HTTP(S) URL using all available hash algorithms and output the result in JSON format")
    args = parser.parse_args()

    niObj = None
    d: dict = None

    casePreference = UriNi.NO_PREFERENCE

    preferShortAlgoId = args.short

    if args.lower:
        casePreference = UriNi.LOWER
    elif args.upper:
        casePreference = UriNi.UPPER

    if args.string != None and args.string != '':
        niObj = UriNi.ofString(args.string, args.algo)

    if args.string == None and args.uri == None and args.filename != None and args.filename != '':
        niObj = UriNi.ofFile(args.filename, args.algo)
        if args.filename != '-':
            mime = mimetypes.guess_type(args.filename)
            if mime[0] != None:
                niObj.setQuery('ct=' + mime[0])
    
    if args.uri != None and args.uri != '':
        niObj = UriNi.ofUri(args.uri)
    
    if args.json_integrity != None and args.json_integrity != '':
        try:
            with open(args.json_integrity, 'r') as jsonFile:
                d = json.load(jsonFile)
                jsonFile.close()
            if not isinstance(d, dict):
                raise ValueError('Top level element needs to be an object')
        except BaseException as e:
            print('Error loading JSON:', e, file=sys.stdout)
            exit(1)
        if args.json_integrity_key != None and args.json_integrity_key != '':
            if args.json_integrity_key != '-':
                d.pop(args.json_integrity_key, None)
        elif args.json_sort_arrays:
            d.pop('integrity_with_sorted_arrays', None)
        else:
            d.pop('integrity', None)
        canonicalJson = canonicalizeJson(d, args.json_sort_arrays)
        niObj = UriNi.ofString(canonicalJson, args.algo)

    if niObj is None and args.remote != None and args.remote != '':
        import requests
        hashers = {}
        headers = {}
        statusCode = 0
        for hashName in sorted(list(UriNi._ni_algo_to_hashlib.keys())):
            try:
                hashers[hashName] = UriNi(algo=hashName)
                if args.authority != None:
                    hashers[hashName].setAuthority(args.authority)
                if args.query != None: # and args.query != '':
                    hashers[hashName].setQuery(args.query)
                #print('created hasher for', hashName, file=sys.stderr)
            except:
                pass # ignore failed hashers, continue with what we got
        #print('start fetching:', args.remote, file=sys.stderr)
        with requests.get(args.remote, timeout=5, stream=True) as response:
            headers = response.headers
            statusCode = response.status_code
            #print('headers:', headers, file=sys.stderr)
            #print('statusCode:', statusCode, file=sys.stderr)
            #print('progress: ', end='', file=sys.stderr, flush=True)
            for chunk in response.iter_content(16384):
                #print('.', end='', file=sys.stderr, flush=True)
                for h in hashers.values():
                    if h != None:
                        try:
                            h._rawUpdate(chunk)
                        except:
                            h = None
                            #print('destroyed hasher for', hashName, file=sys.stderr)
            #print(' done', file=sys.stderr)
        #print('done fetching:', args.remote, file=sys.stderr)
        remoteInfo = {
            'url': args.remote,
            'statusCode': statusCode,
            'headers': dict(headers),
            'integrity': {},
        }
        for hashName, hashNiObj in hashers.items():
            if hashNiObj != None:
                try:
                    hashNiObj._rawFinish()
                    remoteInfo['integrity'][hashName] = hashNiObj.formatNi(preferShortAlgoId=preferShortAlgoId)
                except:
                    pass # ignore failed hashers, continue with what we got

        print(json.dumps(
            remoteInfo,
            indent=2
        ))
        exit()
        
    
    numBytes = 0
    if args.bytes != None and args.bytes != '':
        numBytes = int(args.bytes)

    if args.authority != None: # and args.authority != '':
        niObj.setAuthority(args.authority)

    if args.query != None: # and args.query != '':
        niObj.setQuery(args.query)

    dashes = 4
    if args.dashes != None and args.dashes != '':
        dashes = int(args.dashes)
    
    includeCheckDigit = not args.nocheck

    if args.verify != None and args.verify != '':
        expectedUri = UriNi.ofUri(args.verify)
        expectedUri.setAuthority('')
        expectedUri.setQuery('')
        expectedUriString = expectedUri.formatNi(casePreference=casePreference, preferShortAlgoId=preferShortAlgoId)
        niObj.setAuthority('')
        niObj.setQuery('')
        niObjString = niObj.formatNi(numBytes, casePreference=casePreference, preferShortAlgoId=preferShortAlgoId)
        if niObjString == expectedUriString:
            print('OK: hash value matches', file=sys.stderr)
            exit(0)
        else:
            print('ERROR: hash value DOES NOT match!', file=sys.stderr)
            exit(1)
    elif args.json_integrity != None and args.json_integrity != '':
        formattedUri = None
        if args.nih:
            formattedUri = niObj.formatNih(numBytes, includeCheckDigit, dashes, casePreference=casePreference)
        elif args.nih_b32c:
            formattedUri = niObj.formatNihBase32(numBytes, includeCheckDigit, dashes, True, casePreference=casePreference)
        elif args.nih_b32:
            formattedUri = niObj.formatNihBase32(numBytes, includeCheckDigit, dashes, casePreference=casePreference)
        elif args.syncthing_id:
            formattedUri = niObj.formatSyncthingId()
        else:
            formattedUri = niObj.formatNi(numBytes, casePreference=casePreference, preferShortAlgoId=preferShortAlgoId)
        if args.json_integrity_key != None and args.json_integrity_key != '':
            if args.json_integrity_key == '-':
                print(formattedUri)
                exit()
            else:
                d[args.json_integrity_key] = formattedUri
        elif args.json_sort_arrays:
            d['integrity_with_sorted_arrays'] = formattedUri
        else:
            d['integrity'] = formattedUri
        print(toCompactJSON(d, False))
    elif args.nih:
        print(niObj.formatNih(numBytes, includeCheckDigit, dashes, casePreference=casePreference))
    elif args.wellknown:
        protocol = ''
        if args.protocol != None:
            protocol = args.protocol
        print(niObj.formatWellKnown(protocol, numBytes))
    elif args.wknonstandard:
        protocol = ''
        if args.protocol != None:
            protocol = args.protocol
        filename = 'file.bin'
        if args.filename != None and args.filename != '':
            filename = os.path.basename(args.filename)
        print(niObj.formatWellKnown(protocol, numBytes, True, filename))
    elif args.syncthing_id:
        print(niObj.formatSyncthingId())
    elif args.nih_b32c:
        print(niObj.formatNihBase32(numBytes, includeCheckDigit, dashes, True, casePreference=casePreference))
    elif args.nih_b32:
        print(niObj.formatNihBase32(numBytes, includeCheckDigit, dashes, casePreference=casePreference))
    else:
        print(niObj.formatNi(numBytes, casePreference=casePreference, preferShortAlgoId=preferShortAlgoId))
