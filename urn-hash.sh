#!/bin/sh

# urn:hash as defined in https://datatracker.ietf.org/doc/html/draft-thiemann-hash-urn-01

set -e

UH_FILE="${1}"
UH_ALGO="${2}"
UH_MEDIA_TYPE=""

if [ -n "${UH_FILE}" ] && [ "${UH_FILE}" != "-" ]; then

    # input file is not stdin

    if [ ! -f "${UH_FILE}" ]; then
        echo "First command line argument must be the path to a file" >&2
        exit 1
    fi

    if [ ! -r "${UH_FILE}" ]; then
        echo "File is not readable" >&2
        exit 1
    fi

    UH_MEDIA_TYPE="$(file --brief --mime-type "${UH_FILE}")"
else

    # input file is stdin
    UH_FILE="-"

fi

if [ "${UH_MEDIA_TYPE}" = "application/octet-stream" ]; then
    UH_MEDIA_TYPE=""
fi

get_hash_value_hex() ( # (algo, file) -> hash_value
    UHH_ALGO="${1:?}"
    UHH_FILE="${2:?}"
    case "${UHH_ALGO}" in 
        "blake3")
            printf "%s" "$(b3sum --no-names "${UHH_FILE}")"
            ;;
        "md5")
            printf "%s" "$(md5sum "${UHH_FILE}" | cut -d ' ' -f1)"
            ;;
        "sha1")
            printf "%s" "$(sha1sum "${UHH_FILE}" | cut -d ' ' -f1)"
            ;;
        "sha256")
            printf "%s" "$(sha256sum "${UHH_FILE}" | cut -d ' ' -f1)"
            ;;
        "sha384")
            printf "%s" "$(sha384sum "${UHH_FILE}" | cut -d ' ' -f1)"
            ;;
        "sha512")
            printf "%s" "$(sha512sum "${UHH_FILE}" | cut -d ' ' -f1)"
            ;;
        *)
            echo "Unknown algorithm: ${UHH_ALGO}" >&2
            exit 1
            ;;
    esac
)

case "${UH_ALGO}" in 
    "blake3")
        echo "urn:hash:${UH_MEDIA_TYPE}:blake3:$(get_hash_value_hex blake3 "${UH_FILE}")"
        ;;
    "md5")
        echo "urn:hash:${UH_MEDIA_TYPE}:md5:$(get_hash_value_hex md5 "${UH_FILE}")"
        ;;
    "sha1")
        echo "urn:hash:${UH_MEDIA_TYPE}:sha1:$(get_hash_value_hex sha1 "${UH_FILE}")"
        ;;
    "sha256")
        echo "urn:hash:${UH_MEDIA_TYPE}:sha256:$(get_hash_value_hex sha256 "${UH_FILE}")"
        ;;
    "sha384")
        echo "urn:hash:${UH_MEDIA_TYPE}:sha384:$(get_hash_value_hex sha384 "${UH_FILE}")"
        ;;
    "sha512")
        echo "urn:hash:${UH_MEDIA_TYPE}:sha512:$(get_hash_value_hex sha512 "${UH_FILE}")"
        ;;
    *)
        echo "urn:hash:${UH_MEDIA_TYPE}:sha256:$(get_hash_value_hex sha256 "${UH_FILE}")"
        ;;
esac
